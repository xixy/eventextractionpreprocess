/**
 * @author xixy10@foxmail.com
 * @version V0.1 2018年12月7日 下午4:09:29
 */
package cn.xixy.EventExtraction;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.PropertiesUtils;

/**
 * Created by KrseLee on 2016/11/4.
 */
public class Segmentor {

	// static String file_path =
	// "//Users//apple//Documents//小论文//事件提取//research_code//EventDetection//preprocess//data//tagged_data//original//data_with_one_sentence_range//dev.data.one.sentence.txt";
	// static String out_path =
	// "//Users//apple//Documents//小论文//事件提取//research_code//EventDetection//model//data//dev.id.segmented.txt";
	//
	// static String file_path =
	// "//Users//apple//Documents//小论文//事件提取//research_code//EventDetectionProject//Preprocess//data//tagged_data//original//data_with_one_sentence_range//test.data.one.sentence.txt";
	// static String out_path =
	// "//Users//apple//Documents//小论文//事件提取//research_code//EventDetection//model//data//test.id.segmented.txt";

	// static String file_path =
	// "//Users//apple//Documents//小论文//事件提取//research_code//EventDetectionProject//Preprocess//data//tagged_data//original//data_with_one_sentence_range//train.data.one.sentence.txt";
	// static String out_path =
	// "//Users//apple//Documents//小论文//事件提取//research_code//EventDetectionProject//Preprocess//data//processed//original//train.id.segmented.txt";

	// static String file_path =
	// "//Users//apple//Documents//小论文//事件提取//research_code//EventDetectionProject//Preprocess//data//tagged_data//original//data_with_one_sentence_range//train.data.one.sentence.txt";
	// static String out_path =
	// "//Users//apple//Documents//小论文//事件提取//research_code//EventDetectionProject//Preprocess//data//processed//original//train.id.segmented.txt";

	static StanfordCoreNLP pipline = new StanfordCoreNLP(PropertiesUtils.asProperties("annotators", "tokenize,ssplit",
			"ssplit.isOneSentence", "true", "tokenize.language", "zh", "segment.model",
			"edu/stanford/nlp/models/segmenter/chinese/ctb.gz", "segment.sighanCorporaDict",
			"edu/stanford/nlp/models/segmenter/chinese", "segment.serDictionary",
			"edu/stanford/nlp/models/segmenter/chinese/dict-chris6.ser.gz", "segment.sighanPostProcessing", "true"));

	public void runChineseAnnotators(String text, String out_path) {

		// String text = "克林顿说，华盛顿将逐步落实对韩国的经济援助。"
		// + "金大中对克林顿的讲话报以掌声：克林顿总统在会谈中重申，他坚定地支持韩国摆脱经济危机。";
		Annotation document = new Annotation(text);
		System.out.println(text);

		pipline.annotate(document);
		parserOutput(document, out_path);
	}

	public ArrayList<String> getFileContent(String filePath) {
		ArrayList<String> arrayList = new ArrayList<String>();
		try {
			FileReader fr = new FileReader(filePath);
			BufferedReader bf = new BufferedReader(fr);
			String str;
			String tmp = "";
			// 按行读取字符串
			while ((str = bf.readLine()) != null) {
				if (str.length() == 0) {
					arrayList.add(tmp);
					// System.out.println(tmp);
					tmp = "";
				} else {
					tmp += str.split(" ")[0];
				}

			}
			bf.close();
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// 对ArrayList中存储的字符串进行处理

		return arrayList;

	}

	public void parserOutput(Annotation document, String out_path) {
		// these are all the sentences in this document
		// a CoreMap is essentially a Map that uses class objects as keys and
		// has values with custom types
		List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);

		try {
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out_path, true)));
			for (CoreMap sentence : sentences) {
				// traversing the words in the current sentence
				// a CoreLabel is a CoreMap with additional token-specific
				// methods
				String sent = "";
				for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
					// this is the text of the token
					String word = token.get(CoreAnnotations.TextAnnotation.class);
					// this is the POS tag of the token
					out.write(word + " ");
					// sent += word;
					// System.out.println(word);
				}
				// System.out.println(sent);
			}
			out.newLine();
			out.close();
		} catch (Exception e) {

		}

	}

	public static void segment_articles(String file_path, String out_path) {
		Segmentor example = new Segmentor();
		ArrayList<String> result = example.getFileContent(file_path);
		String text = "";
		for (int i = 0; i < result.size(); i++) {
			text = result.get(i);
			if (text.length() < 1)
				continue;
			example.runChineseAnnotators(result.get(i), out_path);
		}
	}

	public static void main(String[] args) {

		String file_path = "//Users//apple//Documents//小论文//事件提取//research_code//EventDetectionProject//Preprocess//data//tagged_data//original//data_with_one_sentence_range//train.data.one.sentence.txt";
		String out_path = "//Users//apple//Documents//小论文//事件提取//research_code//EventDetectionProject//Preprocess//data//processed//original//train.id.segmented.txt";
		segment_articles(file_path, out_path);
		file_path = "//Users//apple//Documents//小论文//事件提取//research_code//EventDetectionProject//Preprocess//data//tagged_data//original//data_with_one_sentence_range//test.data.one.sentence.txt";
		out_path = "//Users//apple//Documents//小论文//事件提取//research_code//EventDetectionProject//Preprocess//data//processed//original//test.id.segmented.txt";
		segment_articles(file_path, out_path);
		file_path = "//Users//apple//Documents//小论文//事件提取//research_code//EventDetectionProject//Preprocess//data//tagged_data//original//data_with_one_sentence_range//dev.data.one.sentence.txt";
		out_path = "//Users//apple//Documents//小论文//事件提取//research_code//EventDetectionProject//Preprocess//data//processed//original//dev.id.segmented.txt";
		segment_articles(file_path, out_path);

	}
}
