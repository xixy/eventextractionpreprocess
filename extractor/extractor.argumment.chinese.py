#coding=utf-8
import xml.etree.cElementTree as ET
import os
from pathutil import *
import re
from configure import *
from str_util import *
from html.parser import HTMLParser
from split_data import *
from pyltp import SentenceSplitter
import sys

events_number_1 = 0
events_number_2 = 0
events_number_3 = 0

train_sentences_num = 0
dev_sentences_num = 0
test_sentences_num = 0

train_article_num = 0
dev_article_num = 0
test_article_num = 0

convert = False

def convert_punctuation(line):
	'''
	将中文引号转化为英文引号
	'''
	line = line.encode('utf-8')
	line = line.replace('“', '"')
	line = line.replace('”', '"')
	line = line.replace('’', '\'')
	line = line.replace('‘', '\'')

	line = line.decode('utf-8')
	return line

def strQ2B(ustring):
	"""全角转半角"""
	rstring = ""
	for uchar in ustring:
		inside_code=ord(uchar)
		if inside_code == 12288:
		#全角空格直接转换
			inside_code = 32 
		elif (inside_code >= 65281 and inside_code <= 65374):
			#全角字符（除空格）根据关系转化
			inside_code -= 65248

		rstring += unichr(inside_code) # python2
		# rstring += chr(inside_code) # python3

	if rstring != ustring:
		print(ustring)
		print(rstring)
	return rstring

class extractor(object):
	"""从ace原始文件中提取信息"""

	def __init__(self):
		self.ace_2005_data_path = ace_2005_data_dir

		self.train_data_path = train_data_path_cn
		self.test_data_path = test_data_path_cn
		self.dev_data_path = dev_data_path_cn

		self.train_file_mapper = open(train_file_mapper_cn, 'w')
		self.test_file_mapper = open(test_file_mapper_cn, 'w')
		self.dev_file_mapper = open(dev_file_mapper_cn, 'w')

		# train、dev、test数据集所包含的ace文件列表
		self.train_ace_file_list = getFilesForDataSets(ace_npn_train_data_path)
		self.dev_ace_file_list = getFilesForDataSets(ace_npn_dev_data_path)
		self.test_ace_file_list = getFilesForDataSets(ace_npn_test_data_path)

		# 输出文件
		self.train_output_file = open(self.train_data_path,'w')
		self.dev_output_file = open(self.dev_data_path,'w')
		self.test_output_file = open(self.test_data_path,'w')

		# 事件数量
		self.event_num = {
			self.train_output_file:0,
			self.dev_output_file:0,
			self.test_output_file:0
		}
		# 文件数量
		self.article_num = {
			self.train_output_file:0,
			self.dev_output_file:0,
			self.test_output_file:0
		}

	def delete_blank(self, string):
		'''
		去掉所有的空格
		'''
		if type(string) is str:
			return string.replace('\n','').replace(' ','').replace('　', '')
		return string.replace('\n','').replace(' ','').encode('utf-8').replace('　', '')

	def remove_trans(self, line):
		'''
		将XML转义字符
		'''
		return line.replace('&', '&amp;').replace('•','&#8226;')


	def parse_apf_xml(self, file_path='../data/ace_2005_td_v7/data/Chinese/bn/adj/CBS20001006.1000.0074.apf.xml'):
		'''
		从***.apf.xml文件中提取事件
		包括:anchor,extent,ldc_scope,type
		'''
		print(file_path)

		# 事件信息
		events = []
		# 实体信息
		entities = {}
		# timex2信息
		times = {}
		# values信息
		values = {}

		tree = ET.ElementTree(file=file_path)
		root = tree.getroot()
		for elem in tree.iter(tag = 'event'):
			e_type = elem.attrib[event_type]
			e_sub_type = elem.attrib[event_sub_type]
			
			for child in elem:
				if 'event_mention' in child.tag:
					event = {}
					event[event_type] = e_type
					event[event_sub_type] = e_sub_type
					event[ARGUMENTS] = []
					for item in child:
						# 提取anchor
						if anchor in item.tag:
							event[anchor] = self.remove_trans(item[0].text)
							event[anchor_offset] = [int(item[0].attrib[offset_start]),int(item[0].attrib[offset_end])]
						# 提取extent
						if extent in item.tag:
							event[extent] = self.remove_trans(item[0].text)
						# 提取ldc_scope
						if ldc_scope in item.tag:	
							event[ldc_scope] = self.remove_trans(item[0].text)
							event[ldc_scope_offset] = [int(item[0].attrib[offset_start]),int(item[0].attrib[offset_end])]
						# 提取event_mention_argument
						if event_mention_argument in item.tag:
							argument = {}
							argument[role] = item.attrib[role]
							argument[event_mention_argument] = self.remove_trans(item[0][0].text)
							argument[argument_id] = item.attrib[argument_id]
							event[ARGUMENTS].append(argument)

					events.append(event)
		
		# 提取实体信息
		for elem in tree.iter(tag = 'entity'):
			e_type = elem.attrib[entity_type]
			e_sub_type = elem.attrib[entity_sub_type]
			for child in elem:
				# print child.tag
				if child.tag == 'entity_mention':
					entity = {}
					entity[entity_type] = e_type
					entity[entity_sub_type] = e_sub_type
					entity[ENTITY_ID] = child.attrib[ENTITY_ID]
					for item in child:
						if 'extent' in item.tag:
							entity[extent] = self.remove_trans(item[0].text)
							# print(item[0].text)
							entity[extent_offset] = [int(item[0].attrib[entity_offset_start]),int(item[0].attrib[entity_offset_end])]
							# print(entity[extent_offset])
						if 'head' in item.tag:
							entity[head] = self.remove_trans(item[0].text)
							entity[head_offset] = [int(item[0].attrib[head_offset_start]),int(item[0].attrib[head_offset_end])]

					entities[child.attrib[ENTITY_ID]] = entity

		# 提取timex2
		for elem in tree.iter(tag = 'timex2'):
			for child in elem:
				time = {}
				time[TIME_ID] = child.attrib[TIME_ID]
				time[extent] = child[0][0].text
				time[time_offset] = [int(child[0][0].attrib[time_offset_start]),int(child[0][0].attrib[time_offset_end])]
				# times.append(time)
				time[time_type] = time_type_mention
				time[time_sub_type] = time_sub_type_mention
				times[child.attrib[TIME_ID]] = time

		# 提取values
		for elem in tree.iter(tag = 'value'):
			v_type = elem.attrib[value_type]
			# 每个mention
			for child in elem:
				value = {}
				value[VALUE_ID] = child.attrib[VALUE_ID]
				value[value_sub_type] = v_type
				value[value_type] = value_type_mention
				value[value_offset] = [int(child[0][0].attrib[entity_offset_start]),int(child[0][0].attrib[entity_offset_end])]
				value[extent] = child[0][0].text
				# values.append(value)
				values[child.attrib[VALUE_ID]] = value

		return events, entities, times, values

	def construct_dataset_all(self, params):
		"""
		标记事件中的所有论元，用于测试的数据, d1是到trigger的距离，d2全为0
		"""
		file_name = params[0]
		param_units = params[1:]

		events, entities, times, values = self.parse_apf_xml(file_name[:-4]+'.apf.xml')
		output_file = self.get_output_file(file_name)

		# 对事件进行遍历
		for event in events:
			# print(event)
			# 遍历所有的事件
			event_extent = event[extent]
			anchor_start_idx, anchor_end_idx = event[anchor_offset]
			ldc_start_idx, ldc_end_idx = event[ldc_scope_offset]
			event_anchor = event[anchor]
			event_ldc_scope = event[ldc_scope]
			relative_event_start_idx = anchor_start_idx - ldc_start_idx
			relative_event_end_idx = relative_event_start_idx + len(event_anchor) - 1
			event_sub_type_ = event[event_sub_type]

			# 1. 首先找出所有的arguments
			event_arguments = event[ARGUMENTS]

			# print(event_arguments)
			arguments_id = [argument[argument_id] for argument in event_arguments]
			# arguments = 

			# 2. 然后找出来所有的在这个index范围内的实体
			entities_within_ldc_scope = {}
			# times_within_ldc_scope = {}
			# values_within_ldc_scope = {}

			# 2.1 找出所有的实体
			for entity_id, entity in entities.items():
				entity_start_idx, entity_end_idx = entity[extent_offset]
				if entity_start_idx >= ldc_start_idx and entity_end_idx <= ldc_end_idx:
					entities_within_ldc_scope[entity_id] = entity
			# 2.2 找出所有的时间
			for time_id, time in times.items():
				time_start_idx, time_end_idx = time[time_offset]
				if time_start_idx >= ldc_start_idx and time_end_idx <= ldc_end_idx:
					entities_within_ldc_scope[time_id] = time
			# 2.3 找出所有的数值
			for value_id, value in values.items():
				value_start_idx, value_end_idx = value[value_offset]
				if value_start_idx >= ldc_start_idx and value_end_idx <= ldc_end_idx:
					entities_within_ldc_scope[value_id] = value

			# 3. 进行标注
			# 3.0 事件类别
			event_type_ = event[event_type]
			# 3.1 生成字
			tokens = [x for x in event_ldc_scope] # 字的表示
			# 3.2 生成实体标注
			entity_sub_type_annotations = ['O' for _ in tokens]
			entity_type_annotations = ['O' for _ in tokens]
			for entity_id, entity in entities_within_ldc_scope.items():
				entity_start_idx, entity_end_idx = entity[extent_offset]
				# print(entity)
				entity_sub_type_ = entity[entity_sub_type]
				entity_type_ = entity[entity_type]
				relative_start_idx = entity_start_idx - ldc_start_idx
				entity_sub_type_annotations[relative_start_idx] = 'B-' + entity_sub_type_
				entity_type_annotations[relative_start_idx] = 'B-' + entity_type_

				for i in range(1, len(entity[extent])):
					entity_sub_type_annotations[i + relative_start_idx] = 'I-' + entity_sub_type_
					entity_type_annotations[i + relative_start_idx] = 'I-' + entity_type_

			# 3.3 生成argument_role的标记
			argument_annotations = ['O' for _ in tokens]
			
			# 对现有的所有argument进行标注
			argument_annotations = ['O' for _ in tokens]
			for argument in event_arguments:
				arg_id = argument[argument_id]
				arg_role = argument[role]
				argument_start_idx = entities_within_ldc_scope[arg_id][extent_offset][0]
				relative_argument_start_idx = argument_start_idx - ldc_start_idx
				argument_annotations[relative_argument_start_idx] = 'B-' + arg_role
				for i in range(1, len(argument[event_mention_argument])):
					argument_annotations[relative_argument_start_idx + i] = 'I-' + arg_role
			
			d1_annotations = []
			d2_annotations = [0 for _ in tokens]
			# 3.4 生成到trigger的距离
			idx = 0
			for token in tokens:
				entity_sub_type_annotations.append('O')
				entity_type_annotations.append('O')

				# 处理与触发词的相对距离
				# 如果在左边
				if idx < relative_event_start_idx:
					d1_annotations.append(idx - relative_event_start_idx)
				# 如果在右边
				elif idx > relative_event_end_idx:
					d1_annotations.append(idx - relative_event_end_idx)
				# 如果在trigger之间，那么就设为0
				else:
					d1_annotations.append(0)
			




			# 3.5 将\n进行处理
			idx = 0
			new_tokens = []
			new_entity_type_annotations = []
			# type_annotations = []
			new_entity_sub_type_annotations = []
			new_d1_annotations = []
			new_d2_annotations = []
			new_specific_argument_annotations = []

			new_d1 = 0
			new_d2 = 0

			# 先去掉实体的空的部分
			for token, entity_sub_type_, entity_type_, specific_argument_annotation in zip(tokens, entity_sub_type_annotations, entity_type_annotations, argument_annotations):
				if token == '\n' or token == ' ':
					continue
				new_tokens.append(token)
				new_entity_type_annotations.append(entity_type_)
				new_entity_sub_type_annotations.append(entity_sub_type_)
				new_specific_argument_annotations.append(specific_argument_annotation)

			# 对d1进行修改
			idx = -1
			for token in tokens[:relative_event_start_idx][::-1]:
				if token == '\n' or token == ' ':
					continue
				new_d1_annotations.append(idx)
				idx -= 1
			new_d1_annotations = [i for i in reversed(new_d1_annotations)]
			for token in tokens[relative_event_start_idx:relative_event_end_idx + 1]:
				if token == '\n' or token == ' ':
					continue
				new_d1_annotations.append(0)					

			idx = 1
			for token in tokens[relative_event_end_idx:]:
				if token == '\n' or token == ' ':
					continue
				new_d1_annotations.append(idx)
				idx += 1

			new_d2_annotations = [0 for _ in new_d1_annotations]

			# print('\n去掉了空格和换行符的结果\n')

			# 3.3 输出
			output_file.write(event_sub_type_ + ' ' + arg_role + ' ' + event_anchor.replace('\n','').replace(' ','') + '\n')
			for token, entity_sub_type_, entity_type_, argument_role, d1_annotation, d2_annotation, in \
			zip(new_tokens, new_entity_sub_type_annotations, new_entity_type_annotations, new_specific_argument_annotations,  new_d1_annotations, new_d2_annotations):
				output_file.write(token + ' ' + entity_sub_type_ + ' ' +  entity_type_ + ' ' + argument_role + ' ' + str(d1_annotation) + ' ' + str(d2_annotation) + '\n')

			output_file.write('\n')



	def construct_dataset_ltr(self, params):
		"""
		生成从左往右的训练集的数据，包括：
		1. 先对实体进行排序
		2. 对这个句子范围内的实体进行遍历
			2.1 如果实体是论元，就标记输出
			2.2 如果实体不是论元，就设置为O
		"""

		file_name = params[0]
		param_units = params[1:]
		output_file = self.get_output_file(file_name)
		self.article_num[output_file] += 1
		sys.stderr.write("构建:"+file_name+"\n")
		if output_file == self.test_output_file:
			self.construct_dataset_all(params)
			return 0



		events, entities, times, values = self.parse_apf_xml(file_name[:-4]+'.apf.xml')

		# 遍历所有的事件
		for event in events:
			self.event_num[output_file] += 1
			event_arguments = event[ARGUMENTS]
			arguments_id = [argument[argument_id] for argument in event_arguments]
			event_extent = event[extent]
			anchor_start_idx, anchor_end_idx = event[anchor_offset]
			ldc_start_idx, ldc_end_idx = event[ldc_scope_offset]
			event_anchor = event[anchor]
			event_ldc_scope = event[ldc_scope]
			relative_event_start_idx = anchor_start_idx - ldc_start_idx
			relative_event_end_idx = relative_event_start_idx + len(event_anchor) - 1
			event_sub_type_ = event[event_sub_type]

			# 1. 找到这个ldc_scope范围内的所有的实体信息
			entities_within_ldc_scope = {}
			for entity_id, entity in entities.items():
				entity_start_idx, entity_end_idx = entity[extent_offset]
				if entity_start_idx >= ldc_start_idx and entity_end_idx <= ldc_end_idx:
					entities_within_ldc_scope[entity_id] = entity
			# 找出所有的时间
			for time_id, time in times.items():
				time_start_idx, time_end_idx = time[time_offset]
				if time_start_idx >= ldc_start_idx and time_end_idx <= ldc_end_idx:
					entities_within_ldc_scope[time_id] = time
			# 找出所有的数值
			for value_id, value in values.items():
				value_start_idx, value_end_idx = value[value_offset]
				if value_start_idx >= ldc_start_idx and value_end_idx <= ldc_end_idx:
					entities_within_ldc_scope[value_id] = value

			# 2. 按照offset进行排序
			entities_by_index = sorted(entities_within_ldc_scope.values(), key = lambda x : x[extent_offset][0])

			# 3. 现在处理实体，循环进行处理
			event_type_ = event[event_type]

			# 3.1 生成字
			tokens = [x for x in event_ldc_scope] # 字的表示
			# 3.2 生成实体标注
			entity_sub_type_annotations = ['O' for _ in tokens]
			entity_type_annotations = ['O' for _ in tokens]
			for entity_id, entity in entities_within_ldc_scope.items():
				entity_start_idx, entity_end_idx = entity[extent_offset]
				# print(entity)
				entity_sub_type_ = entity[entity_sub_type]
				entity_type_ = entity[entity_type]
				relative_start_idx = entity_start_idx - ldc_start_idx
				entity_sub_type_annotations[relative_start_idx] = 'B-' + entity_sub_type_
				entity_type_annotations[relative_start_idx] = 'B-' + entity_type_

				for i in range(1, len(entity[extent])):
					entity_sub_type_annotations[i + relative_start_idx] = 'I-' + entity_sub_type_
					entity_type_annotations[i + relative_start_idx] = 'I-' + entity_type_
			# 3.3 生成argument_role的标记
			argument_annotations = ['O' for _ in tokens]

			'''
			3.3 针对实体生成论元标注，两种情况
				1. 如果实体是论元，就标记输出
				2. 如果实体不是论元，就设置为O
			'''

			for entity in entities_by_index:
				entity_start_idx, entity_end_idx = entity[extent_offset]
				relative_entity_start_idx = entity_start_idx - ldc_start_idx
				relative_entity_end_idx = entity_end_idx - ldc_start_idx

				# 处理distance的标注
				d1_annotations = []
				d2_annotations = []

				idx = 0
				for _ in tokens:

					# 处理与触发词的相对距离
					# 如果在左边
					if idx < relative_event_start_idx:
						d1_annotations.append(idx - relative_event_start_idx)
					# 如果在右边
					elif idx > relative_event_end_idx:
						d1_annotations.append(idx - relative_event_end_idx)
					# 如果在trigger之间，那么就设为0
					else:
						d1_annotations.append(0)

					# 处理与argument candidate 的相对距离
					# 如果在左边
					if idx < relative_entity_start_idx:
						d2_annotations.append(idx - relative_entity_start_idx)
					# 如果在右边
					elif idx > relative_entity_end_idx:
						d2_annotations.append(idx - relative_entity_end_idx)
					# 如果在trigger之间，那么就设为0
					else:
						d2_annotations.append(0)
					idx += 1

				# 查看该entity是否是argument
				# 如果该entity是arguments
				if entity[ENTITY_ID] in arguments_id:
					# 1. 获取argument role
					argument = event_arguments[arguments_id.index(entity[ENTITY_ID])]
					arg_role = argument[role]
				# 如果不在argument中
				else:
					arg_role = 'O'

				# 进行输出
				# 3.2 将\n进行处理
				idx = 0
				new_tokens = []
				new_entity_type_annotations = []
				# type_annotations = []
				new_entity_sub_type_annotations = []
				new_d1_annotations = []
				new_d2_annotations = []
				new_specific_argument_annotations = []

				new_d1 = 0
				new_d2 = 0

				# 先去掉实体的空的部分
				for token, entity_sub_type_, entity_type_, specific_argument_annotation in zip(tokens, entity_sub_type_annotations, entity_type_annotations, argument_annotations):
					if token == '\n' or token == ' ':
						continue
					new_tokens.append(token)
					new_entity_type_annotations.append(entity_type_)
					new_entity_sub_type_annotations.append(entity_sub_type_)
					new_specific_argument_annotations.append(specific_argument_annotation)

				# 对d1进行修改
				idx = -1
				for token in tokens[:relative_event_start_idx][::-1]:
					if token == '\n' or token == ' ':
						continue
					new_d1_annotations.append(idx)
					idx -= 1
				new_d1_annotations = [i for i in reversed(new_d1_annotations)]
				for token in tokens[relative_event_start_idx:relative_event_end_idx + 1]:
					if token == '\n' or token == ' ':
						continue
					new_d1_annotations.append(0)					

				idx = 1
				for token in tokens[relative_event_end_idx:]:
					if token == '\n' or token == ' ':
						continue
					new_d1_annotations.append(idx)
					idx += 1

				# 对d2进行修改
				idx = -1
				for token in tokens[:relative_entity_start_idx][::-1]:
					if token == '\n' or token == ' ':
						continue
					new_d2_annotations.append(idx)
					idx -= 1	
				new_d2_annotations = [i for i in reversed(new_d2_annotations)]
				for token in tokens[relative_entity_start_idx:relative_entity_end_idx + 1]:
					if token == '\n' or token == ' ':
						continue
					new_d2_annotations.append(0)				
				idx = 1
				for token in tokens[relative_entity_end_idx:]:
					if token == '\n' or token == ' ':
						continue
					new_d2_annotations.append(idx)
					idx += 1

				# print('\n去掉了空格和换行符的结果\n')

				# 3.3 输出
				output_file.write(event_sub_type_ + ' ' + arg_role + ' ' \
					+ event_anchor.replace('\n','').replace(' ','') + ' ' + entity[extent].replace('\n','').replace(' ','') +  '\n')
				for token, entity_sub_type_, entity_type_, argument_role, d1_annotation, d2_annotation, in \
				zip(new_tokens, new_entity_sub_type_annotations, new_entity_type_annotations, new_specific_argument_annotations,  new_d1_annotations, new_d2_annotations):
					output_file.write(token + ' ' + entity_sub_type_ + ' ' +  entity_type_ + ' ' + argument_role + ' ' + str(d1_annotation) + ' ' + str(d2_annotation) + '\n')

				output_file.write('\n')

				# 处理argument role的annotation，让下一轮进行使用
				if arg_role != 'O':
					argument_annotations[relative_entity_start_idx] = 'B-' + arg_role
					for i in range(relative_entity_start_idx + 1, relative_entity_end_idx + 1):
						argument_annotations[i] = 'I-' + arg_role











	def construct_dataset(self, params):
		'''
		生成训练集
		Args:
			params: 某文件所有段落的信息，包括[file_name, [start_idx, end_idx, param]]
		'''
		file_name = params[0]
		param_units = params[1:]

		events, entities, times, values = self.parse_apf_xml(file_name[:-4]+'.apf.xml')
		output_file = self.get_output_file(file_name)

		# 对事件进行遍历
		for event in events:
			# print(event)
			# 遍历所有的事件
			event_extent = event[extent]
			anchor_start_idx, anchor_end_idx = event[anchor_offset]
			ldc_start_idx, ldc_end_idx = event[ldc_scope_offset]
			event_anchor = event[anchor]
			event_ldc_scope = event[ldc_scope]
			relative_event_start_idx = anchor_start_idx - ldc_start_idx
			relative_event_end_idx = relative_event_start_idx + len(event_anchor) - 1
			event_sub_type_ = event[event_sub_type]

			# 1. 首先找出所有的arguments
			event_arguments = event[arguments]

			# # 1.1 对arguments进行排序，按照在句子中的
			# event_arguments = sorted(event_arguments, key = lambda x :[x[argument_id]][extent_offset][0])

			# print(event_arguments)
			arguments_id = [argument[argument_id] for argument in event_arguments]
			# arguments = 

			# 2. 然后找出来所有的在这个index范围内的实体
			entities_within_ldc_scope = {}
			# times_within_ldc_scope = {}
			# values_within_ldc_scope = {}

			# 2.1 找出所有的实体
			for entity_id, entity in entities.items():
				entity_start_idx, entity_end_idx = entity[extent_offset]
				if entity_start_idx >= ldc_start_idx and entity_end_idx <= ldc_end_idx:
					entities_within_ldc_scope[entity_id] = entity
			# 2.2 找出所有的时间
			for time_id, time in times.items():
				time_start_idx, time_end_idx = time[time_offset]
				if time_start_idx >= ldc_start_idx and time_end_idx <= ldc_end_idx:
					entities_within_ldc_scope[time_id] = time
			# 2.3 找出所有的数值
			for value_id, value in values.items():
				value_start_idx, value_end_idx = value[value_offset]
				if value_start_idx >= ldc_start_idx and value_end_idx <= ldc_end_idx:
					entities_within_ldc_scope[value_id] = value

			# 3. 进行标注
			event_type_ = event[entity_type]
			tokens = [x for x in event_ldc_scope] # 字的信息
			
			# 对现有的所有argument进行标注
			argument_annotations = ['O' for _ in tokens]
			for argument in event_arguments:
				arg_id = argument[argument_id]
				arg_role = argument[role]
				argument_start_idx = entities_within_ldc_scope[arg_id][extent_offset][0]
				relative_argument_start_idx = argument_start_idx - ldc_start_idx
				argument_annotations[relative_argument_start_idx] = 'B-' + arg_role
				for i in range(1, len(argument[event_mention_argument])):
					argument_annotations[relative_argument_start_idx + i] = 'I-' + arg_role

			# 遍历所有的arguments，每个argument都需要构建相应的一个句子
			for argument in event_arguments:
				arg_id = argument[argument_id]
				arg_role = argument[role]
				# 生成entity向量
				entity_type_annotations = []
				# type_annotations = []
				entity_sub_type_annotations = []
				d1_annotations = []
				d2_annotations = []

				argument_start_idx = entities_within_ldc_scope[arg_id][extent_offset][0]
				relative_argument_start_idx = argument_start_idx - ldc_start_idx
				relative_argument_end_idx = relative_argument_start_idx + len(argument[event_mention_argument]) - 1

				specific_argument_annotations = []
				specific_argument_annotations.extend(argument_annotations) # 这条数据的特定的argument的标记
				# 去掉当前的argument，这是需要预测而且是未知的
				for i in range(len(argument[event_mention_argument])):
					specific_argument_annotations[relative_argument_start_idx + i] = 'O'
	
				idx = 0
				for token in tokens:
					entity_sub_type_annotations.append('O')
					entity_type_annotations.append('O')

					# 处理与触发词的相对距离
					# 如果在左边
					if idx < relative_event_start_idx:
						d1_annotations.append(idx - relative_event_start_idx)
					# 如果在右边
					elif idx > relative_event_end_idx:
						d1_annotations.append(idx - relative_event_end_idx)
					# 如果在trigger之间，那么就设为0
					else:
						d1_annotations.append(0)

					# 处理与argument candidate 的相对距离
					# 如果在左边
					if idx < relative_argument_start_idx:
						d2_annotations.append(idx - relative_argument_start_idx)
					# 如果在右边
					elif idx > relative_argument_end_idx:
						d2_annotations.append(idx - relative_argument_end_idx)
					# 如果在trigger之间，那么就设为0
					else:
						d2_annotations.append(0)
					idx += 1


					# sub_type_annotations.append('O')

				# 先处理所有的非argument的部分，然后在处理argument的部分，因为可能产生覆盖
				for entity_id, entity in entities_within_ldc_scope.items():
					if entity_id in arguments_id:
						continue
					entity_start_idx, entity_end_idx = entity[extent_offset]
					# print(entity)
					entity_sub_type_ = entity[entity_sub_type]
					entity_type_ = entity[entity_type]
					relative_start_idx = entity_start_idx - ldc_start_idx
					entity_sub_type_annotations[relative_start_idx] = 'B-' + entity_sub_type_
					entity_type_annotations[relative_start_idx] = 'B-' + entity_type_

					for i in range(1, len(entity[extent])):
						entity_sub_type_annotations[i + relative_start_idx] = 'I-' + entity_sub_type_
						entity_type_annotations[i + relative_start_idx] = 'I-' + entity_type_

				# 然后处理argument的entities
				for entity_id, entity in entities_within_ldc_scope.items():
					if entity_id not in arguments_id:
						continue
					entity_start_idx, entity_end_idx = entity[extent_offset]
					# print(entity)
					entity_sub_type_ = entity[entity_sub_type]
					entity_type_ = entity[entity_type]
					relative_start_idx = entity_start_idx - ldc_start_idx
					entity_sub_type_annotations[relative_start_idx] = 'B-' + entity_sub_type_
					entity_type_annotations[relative_start_idx] = 'B-' + entity_type_

					for i in range(1, len(entity[extent])):
						entity_sub_type_annotations[i + relative_start_idx] = 'I-' + entity_sub_type_
						entity_type_annotations[i + relative_start_idx] = 'I-' + entity_type_


				# 3.2 将\n进行处理
				idx = 0
				new_tokens = []
				new_entity_type_annotations = []
				# type_annotations = []
				new_entity_sub_type_annotations = []
				new_d1_annotations = []
				new_d2_annotations = []
				new_specific_argument_annotations = []

				new_d1 = 0
				new_d2 = 0

				# 先去掉实体的空的部分
				for token, entity_sub_type_, entity_type_, specific_argument_annotation in zip(tokens, entity_sub_type_annotations, entity_type_annotations, specific_argument_annotations):
					if token == '\n' or token == ' ':
						continue
					new_tokens.append(token)
					new_entity_type_annotations.append(entity_type_)
					new_entity_sub_type_annotations.append(entity_sub_type_)
					new_specific_argument_annotations.append(specific_argument_annotation)

				# 对d1进行修改
				idx = -1
				for token in tokens[:relative_event_start_idx][::-1]:
					if token == '\n' or token == ' ':
						continue
					new_d1_annotations.append(idx)
					idx -= 1
				new_d1_annotations = [i for i in reversed(new_d1_annotations)]
				for token in tokens[relative_event_start_idx:relative_event_end_idx + 1]:
					if token == '\n' or token == ' ':
						continue
					new_d1_annotations.append(0)					

				idx = 1
				for token in tokens[relative_event_end_idx:]:
					if token == '\n' or token == ' ':
						continue
					new_d1_annotations.append(idx)
					idx += 1

				# 对d2进行修改
				idx = -1
				for token in tokens[:relative_argument_start_idx][::-1]:
					if token == '\n' or token == ' ':
						continue
					new_d2_annotations.append(idx)
					idx -= 1	
				new_d2_annotations = [i for i in reversed(new_d2_annotations)]
				for token in tokens[relative_argument_start_idx:relative_argument_end_idx + 1]:
					if token == '\n' or token == ' ':
						continue
					new_d2_annotations.append(0)				
				idx = 1
				for token in tokens[relative_argument_end_idx:]:
					if token == '\n' or token == ' ':
						continue
					new_d2_annotations.append(idx)
					idx += 1

				# print('\n去掉了空格和换行符的结果\n')

				# 3.3 输出
				output_file.write(event_sub_type_ + ' ' + arg_role + ' ' + event_anchor.replace('\n','').replace(' ','') + '\n')
				for token, entity_sub_type_, entity_type_, argument_role, d1_annotation, d2_annotation, in \
				zip(new_tokens, new_entity_sub_type_annotations, new_entity_type_annotations, new_specific_argument_annotations,  new_d1_annotations, new_d2_annotations):
					output_file.write(token + ' ' + entity_sub_type_ + ' ' +  entity_type_ + ' ' + argument_role + ' ' + str(d1_annotation) + ' ' + str(d2_annotation) + '\n')

				output_file.write('\n')

	def check_offsets(self, params):
		'''
		提取实体信息，并且将实体信息与句子进行对齐，进行标记
		Args:
			params: 某文件所有段落的信息，包括[file_name, [start_idx, end_idx, param]]
		'''
		# entity_annotations = []

		file_name = params[0]
		param_units = params[1:]
		output = False

		events, entities, times, values = self.parse_apf_xml(file_name[:-4]+'.apf.xml')
		# 1. 检查实体信息
		for entity in entities.values():
			found = False
			# print(entity)
			extent_start_idx = entity[extent_offset][0]
			extent_end_idx = entity[extent_offset][1]
			entity_extent = entity[extent]
			# print(entity_extent)

			# 找到实体所在的段落
			for param_unit in param_units:
				param_start_idx = param_unit[0]
				param_end_idx = param_unit[1]
				if param_start_idx <= extent_start_idx and param_end_idx >= extent_end_idx:
					relative_idx = extent_start_idx - param_start_idx
					param = param_unit[2]
					param_entity = param[relative_idx:relative_idx + len(entity_extent)]

					# print(param_entity)
					if param_entity != entity_extent:
						# 加二
						if output == False:
							print(file_name)
							output = True
						print('---')
						print(entity)
						print('---')
						print(entity_extent)
						print('---')
						print(param_entity)
						print('---')
						print(param)
						print('---')

						print(param_units)
						raise Exception('出现错误')
					found = True
			if found == False:
				print(entity)
				raise Exception('存在实体没有找到对应位置')

		# 2. 检查事件信息
		for event in events:
			found = False
			# print(event)
			anchor_start_idx, anchor_end_idx = event[anchor_offset]

			event_anchor = event[anchor]
			for param_unit in param_units:
				param_start_idx = param_unit[0]
				param_end_idx = param_unit[1]
				# print(param_start_idx)

				if param_start_idx <= anchor_start_idx and param_end_idx >= anchor_end_idx:
					relative_idx = anchor_start_idx - param_start_idx
					param = param_unit[2]
					param_anchor = param[relative_idx:relative_idx + len(event_anchor)]

					if param_anchor != event_anchor:
						print('---')
						print(event)
						print('---')
						print(param_anchor)
						print('---')
						print(param)
						print('---')
						print(param_units)
						raise Exception('出现错误')
					found = True

			if found == False:
				print(entity)
				raise Exception('存在事件没有找到对应位置')

		# 3. 检查时间信息
		for time in times.values():
			found = False
			# print(event)
			time_start_idx, time_end_idx = time[time_offset]

			time_extent = time[extent]
			for param_unit in param_units:
				param_start_idx = param_unit[0]
				param_end_idx = param_unit[1]
				# print(param_start_idx)

				if param_start_idx <= time_start_idx and param_end_idx >= time_end_idx:
					relative_idx = time_start_idx - param_start_idx
					param = param_unit[2]
					param_anchor = param[relative_idx:relative_idx + len(time_extent)]

					if param_anchor != time_extent:
						print('---')
						print(time)
						print('---')
						print(param_anchor)
						print('---')
						print(param)
						print('---')
						print(param_units)
						raise Exception('出现错误')
					found = True

			if found == False and time_start_idx > param_units[0][0] and len(time_extent) != len('2004-12-04T14:08:00')  \
				and len(time_extent) != len('2004-12-19T09:44:53 CST'):
				print(time)
				print(param_units)
				raise Exception('存在时间没有找到对应位置')	

		# 4. 检查数值信息
		for value in values.values():
			found = False
			# print(event)
			value_start_idx, value_end_idx = value[value_offset]

			value_extent = value[extent]
			for param_unit in param_units:
				param_start_idx = param_unit[0]
				param_end_idx = param_unit[1]
				# print(param_start_idx)

				if param_start_idx <= value_start_idx and param_end_idx >= value_end_idx:
					relative_idx = value_start_idx - param_start_idx
					param = param_unit[2]
					param_anchor = param[relative_idx:relative_idx + len(value_extent)]

					if param_anchor != value_extent:
						print('---')
						print(value)
						print('---')
						print(param_anchor)
						print('---')
						print(param)
						print('---')
						print(param_units)
						raise Exception('出现错误')
					found = True

			if found == False:
				print(value)
				raise Exception('存在数值没有找到对应位置')



	def extract_param(self):
		'''
		从sgm文件中提取段落文本, 每段用空格隔开，然后拼接成一行，去掉空格和URL
		'''

		# 首先从sgm文件中提取文本，存入到middle_result.dat文件中
		count = 0
		files_params = []

		# 1. 先处理 bn 部分
		folder = self.ace_2005_data_path + '/' + 'data/Chinese/bn/adj/'
		sgm_files = getFilePath(folder, 'sgm')

		for sgm_file in sgm_files:
			# if 'CNR20001121.1700.1232' not in sgm_file:
			# 	continue
			start_idx, end_idx = 0, 0 #用来对段落的idx进行描述
			params = [] # 存储这个sgm文件的段落内容
			params.append(sgm_file)
			param = ''
			param_unit = [] # 存储一个param的信息
			in_param = False
			tree = ET.ElementTree(file = sgm_file)
			root = tree.getroot()
			for elem in root[:3]:
				start_idx += len(elem.text)

			with open(sgm_file) as f:
				spaces = 0 # 空格的大小，在<TRUN>可能会出现
				for line in f:
					start_idx += 1
					# print(start_idx)
					# print(start_idx)
					if '<TURN>' in line:
						param = ''
						param_unit = []
						spaces = len(line.replace('<TURN>', '')) - 1
						# print(spaces)
						# 计算start_idx
						param_unit.append(start_idx)
						in_param = True
						continue

					if '</TURN>' in line:
						in_param = False
						# 计算end_idx
						if param == '':
							start_idx += spaces
							spaces = 0
							continue
						param_unit.append(start_idx - 1)
						param_unit.append(param)
						params.append(param_unit)

						param = ''
						param_unit = []

					if in_param:

						param += line
						start_idx -= 1
						start_idx += len(line)
			files_params.append(params)


		# 2. 处理 nw 部分
		folder = self.ace_2005_data_path + '/' + 'data/Chinese/nw/adj/'
		sgm_files = getFilePath(folder, 'sgm')

		for sgm_file in sgm_files:
			# if 'XIN20001001.1400.0096' not in sgm_file:
			# 	continue
			start_idx, end_idx = 0, 0 #用来对段落的idx进行描述
			params = [] # 存储这个sgm文件的段落内容
			params.append(sgm_file)
			param = ''
			param_unit = [] # 存储一个param的信息
			in_param = False
			tree = ET.ElementTree(file = sgm_file)
			root = tree.getroot()
			for elem in root[:3]:
				start_idx += len(elem.text)
			# 加入HEADLINE的信息
			# print(root[3][0].text)
			# print(len(root[3][0].text))
			start_idx += len(root[3][0].text) - 2

			with open(sgm_file) as f:
				spaces = 0 # 空格的大小，在<TRUN>可能会出现
				for line in f:
					start_idx += 1
					# print(start_idx)
					# print(start_idx)
					if '<TEXT>' in line:
						param = ''
						param_unit = []
						spaces = len(line.replace('<TEXT>', '')) - 1
						# print(spaces)
						# 计算start_idx
						param_unit.append(start_idx)
						in_param = True
						continue

					if '</TEXT>' in line:
						in_param = False
						# 计算end_idx
						if param == '':
							start_idx += spaces
							spaces = 0
							continue
						param_unit.append(start_idx - 1)
						param_unit.append(param)
						params.append(param_unit)

						param = ''
						param_unit = []

					if in_param:

						param += line
						start_idx -= 1
						start_idx += len(line)
			files_params.append(params)


		# 3. 处理 wl 部分
		folder = self.ace_2005_data_path + '/' + 'data/Chinese/wl/adj/'
		sgm_files = getFilePath(folder, 'sgm')

		for sgm_file in sgm_files:
			# if 'DAVYZW_20050111.1514' not in sgm_file:
			# 	continue
			start_idx, end_idx = 0, 0 #用来对段落的idx进行描述
			params = [] # 存储这个sgm文件的段落内容
			params.append(sgm_file)
			param = ''
			param_unit = [] # 存储一个param的信息
			in_param = False
			tree = ET.ElementTree(file = sgm_file)
			root = tree.getroot()
			for elem in root[:3]:
				start_idx += len(elem.text)
			# 加入HEADLINE的信息
			# print(root[3][0].text)
			# print(len(root[3][0].text))
			start_idx += len(self.remove_trans(root[3][0].text)) - 2

			# 

			with open(sgm_file) as f:
				spaces = 0 # 空格的大小，在<TRUN>可能会出现
				for line in f:
					start_idx += 1
					# print(start_idx)
					# print(start_idx)
					if '<POSTER>' in line:
						# print(line[8:][:-10])
						param_unit = []
						param_unit.append(start_idx - 1)


						start_idx += len(line[8:][:-10])
						param_unit.append(start_idx)
						param_unit.append(line[8:][:-10])
						params.append(param_unit)
						param_unit = []

					if '<POSTDATE>' in line:
						# print(line[10:][:-12])
						start_idx += len(line[10:][:-12])
						in_param = True
						param_unit.append(start_idx)
						continue
					# 结束
					if '</POST>' in line:
						in_param = False
						# 计算end_idx
						if param == '':
							start_idx += spaces
							spaces = 0
							continue
						param_unit.append(start_idx - 1)
						param_unit.append(param)
						params.append(param_unit)

						param = ''
						param_unit = []

					if in_param:
						param += line
						start_idx -= 1
						start_idx += len(line)
			files_params.append(params)

		return files_params

	def get_output_file(self, line):
		'''
		根据文件列表名来判断应该输出到train、dev、test哪个文件
		Args:
			line:../../data/ace_2005_td_v7/data/Chinese/bn/adj/CTS20001218.1300.0965.sgm
		'''
		name = line.split('/')[-1][:-4]
		# print(line)
		# print(name)
		if name in self.train_ace_file_list:
			return self.train_output_file
		elif name in self.test_ace_file_list:
			return self.test_output_file
		elif name in self.dev_ace_file_list:
			return self.dev_output_file
		else:
			print(line)
			print(name)
			return self.train_output_file
			# raise Exception('找不到归类')
					

if __name__ == '__main__':
	ext = extractor()
	# 从sgm文件中提取段落
	file_params = ext.extract_param()
	sys.stderr.write("段落提取结束\n")	
	# 对文件进行遍历
	for file_param in file_params:
		ext.check_offsets(file_param)
		# ext.construct_dataset(file_param)
		ext.construct_dataset_ltr(file_param)
		# break
	sys.stderr.write("处理得到%d个文件\n" % (sum(ext.article_num.values())))
	sys.stderr.write("处理得到%d个事件\n" % (sum(ext.event_num.values())))

	sys.stderr.write(str(ext.article_num))






		