#coding=utf-8

import ace_data
data = ace_data.load()

docs = {}
relation2id = {'NA': 0}
entity_count = 0
relation_count = 0
relation_count_dict = {}
arg1_first_count = 0
symmetric_count = 0
contain_count = 0
for doc_id, text in data["docs"].items():
    entities = {}
    relations = {}
    text = re.sub('\d', '0', text)
    text = text.lower()
    docs[doc_id] = {
        'text': text,
        'entities': entities,
        'relations': relations
    }
    for entity_id, entity in data["nes"][doc_id].items():
        entity_count += 1
        entities[entity_id] = {
            'id': entity[0],
            'type': entity[1],
            'doc_position': [entity[2], entity[3]],
            'name': entity[4],
            'subtype': entity[5],
            'doc_id': doc_id
        }
    for relation_id, relation in data["res"][doc_id].items():
        relation_count += 1
        if relation[1] not in relation_count_dict:
            relation_count_dict[relation[1]] = 0
        relation_count_dict[relation[1]] += 1
        if relation[1] not in relation2id:
            relation2id[relation[1]] = len(relation2id)
        if (relation[1] == 'PHYS' and relation[2] == 'Near') or relation[1] == 'PER-SOC':
            symmetric_count += 1
            arg1, arg2 = entities[relation[4]], entities[relation[5]]
            if arg1['doc_position'][0] <= arg2['doc_position'][0]:
                arg1_first_count += 1
        arg1, arg2 = entities[relation[4]], entities[relation[5]]
        if (arg1['doc_position'][0] >= arg2['doc_position'][0] and arg1['doc_position'][0] <= arg2['doc_position'][1]) \
            or (arg1['doc_position'][1] >= arg2['doc_position'][0] and arg1['doc_position'][1] <= arg2['doc_position'][1]):
            contain_count += 1
            
#         if relation[1] == 'PHYS':
#             arg1, arg2 = entities[relation[4]], entities[relation[5]]
#             if arg1['doc_position'][1] <= arg2['doc_position'][0]:
#                 arg1_first_count += 1
        relations[relation_id] = {
            'id': relation[0],
            'type': relation[1],
            'arg1_id': relation[4],
            'arg2_id': relation[5],
            'doc_id': doc_id
        }
print(entity_count)
print(relation_count)
print(relation2id)
print(relation_count_dict)
total = np.sum(np.array(list(relation_count_dict.values())))
print(total, symmetric_count, arg1_first_count, symmetric_count / total)
print(contain_count, contain_count / total)

def init_sentences(docs, char2id, pos2id, entity2id, word2id, is_train=True):
    '''
    初始化句子信息，并生成词性和字符特征
    Args:
        docs: docs
    Returns:
        char2id,
        pos2id,
        docs: {
            doc_id: {
                ...
                sentences: [{ 'start': 0, 'end': 0,  'index': 0, 'text': '' , chars = [chars], pos = [pos]}, ...]
            }
        }
    '''
    sentence_end_marks = ['。', '！', '!', '？', '?']
    # for each doc
    for doc_id, doc in docs.items():
#         if not doc_id.startswith('9'): continue
        sentences = []
        doc['sentences'] = sentences
        tmp_sentence = { 'start': 0, 'end': 0,  'index': 0, 'text': '', 'doc_id': doc_id}
        char_index = 0
        char_index2sentence_index = {}
        for char in doc['text']:
            char_index2sentence_index[char_index] = [len(sentences), len(tmp_sentence['text'])]
            if char != '\n': 
                tmp_sentence['text'] += char
                if char in sentence_end_marks:
                    tmp_sentence['end'] = char_index
                    tmp_sentence['index'] = len(sentences)
                    sentences.append(tmp_sentence)
                    tmp_sentence = { 'start': char_index + 1, 'end': 0,  'index': 0, 'text': '', 'doc_id': doc_id }
            char_index += 1
        # 加入最后的句子
        if len(tmp_sentence['text']) > 0 and tmp_sentence['text'][-1] not in sentence_end_marks:
            tmp_sentence['end'] = char_index
            tmp_sentence['index'] = len(sentences)
            sentences.append(tmp_sentence)
        # 每个句子进行标注为0
        for sentence in sentences:
            sentence['labels'] = [0] * len(sentence['text'])
        # 每个实体进行标注
        for entity in doc['entities'].values():
            start_position = char_index2sentence_index[entity['doc_position'][0]]
            end_position = char_index2sentence_index[entity['doc_position'][1]]
            entity['sentence_index'] = start_position[0]
            sentence = sentences[start_position[0]]
            entity['sentence_position'] = [start_position[1], end_position[1]]
            start_type = 'B-' +  entity['type']
            in_type = 'I-' +  entity['type']
            if start_type not in entity2id: entity2id[start_type] = len(entity2id)
            if in_type not in entity2id: entity2id[in_type] = len(entity2id)
            sentence['labels'][start_position[1]] = entity2id[start_type]
            for i in range(start_position[1] + 1, end_position[1]):
                sentence['labels'][i] = entity2id[in_type]
        
        for sentence in sentences:
            chars = []
            for char in sentence['text']:
                if is_train:
                    if char not in char2id: char2id[char] = len(char2id)
                    chars.append(char2id[char])
                else:
                    chars.append(char2id[char] if char in char2id else 0)
            words = []
            word_pos = []
            pos = []
            segments = []
            charindex2wordindex = {}
            charindex = 0
            wordindex = 0
            for word, flag in pseg.cut(sentence['text']):
                if flag not in pos2id: pos2id[flag] = len(pos2id) 
                if is_train:
                    if word not in word2id: word2id[word] = len(word2id)
                    words.append(word2id[word])
                else:
                    words.append(word2id[word] if word in word2id else 0)
                pos.extend([pos2id[flag]] * len(word))
                if len(word) == 1:
                    segments.append(0)
                    charindex2wordindex[charindex] = wordindex
                    charindex += 1
                else:
                    tmp = [2] * len(word)
                    tmp[0] = 1
                    tmp[-1] = 3
                    segments.extend(tmp)
                    for i in range(len(word)):
                        charindex2wordindex[charindex + i] = wordindex
                    charindex += len(word)
                wordindex += 1
            sentence['chars'] = chars
            sentence['segments'] = segments
            sentence['pos'] = pos
            sentence['words'] = words
            sentence['charindex2wordindex'] = charindex2wordindex
    return docs


char2id = {'unk': 0}
word2id = {'unk': 0}
pos2id = {'unk': 0}
entity2id = {'O': 0}
init_sentences(docs, char2id, pos2id, entity2id, word2id, is_train=True)
print(len(entity2id))
print(len(char2id))
print(len(pos2id))

max_sentence_gap = 1
max_char_gap = 80

def get_instance_id_by_entities(entity1, entity2):
    return entity1['doc_id'] + '-' + entity1['id'] + '-' + entity2['id']

def get_instance_id_by_relation(relation):
    return relation['doc_id'] + '-' + relation['arg1_id'] + '-' + relation['arg2_id']
        
def is_active_instance(entity1, entity2, instances):
    instance_id = get_instance_id_by_entities(entity1, entity2)
    return instance_id in instances

def get_sentence_by_position(position, sentences):
    for sentence in sentences:
        if position >= sentence['start'] and position <= sentence['end'] :
            return sentence
    return False

def get_position_feature(position, entity_position):
    if position < entity_position[0]:
        return position - entity_position[0]
    elif position >= entity_position[1]:
        return position - entity_position[1] + 1
    else:
        return 0

def feature_length_filter(pf1, pf2, arg1_pos, arg2_pos, index, cropping = False):
    if cropping:
        pos_min, pos_max = min(arg1_pos[0], arg1_pos[1], arg2_pos[0], arg2_pos[1]), max(arg1_pos[0], arg1_pos[1], arg2_pos[0], arg2_pos[1])
        return pos_min <= index < pos_max
    else:
        return abs(pf1[index]) <= 127 and abs(pf2[index]) <= 127

def get_instance_features(sentence_indexes, sentences, arg1, arg2, cropping = False):
    words = []
    for sentence_index in sentence_indexes:
        sentence = sentences[sentence_index]
        words.extend(sentence['words'])

    start_sentence = sentences[sentence_indexes[0]]
    end_sentence = sentences[sentence_indexes[-1]]
    tmp_pf1, tmp_pf2 = [], []
    try:
        arg1_pos = [sentences[arg1['sentence_index']]['charindex2wordindex'][p] for p in arg1['sentence_position']]
        arg2_pos = [sentences[arg2['sentence_index']]['charindex2wordindex'][p] for p in arg2['sentence_position']]
    except Exception as e:
        return [], [], []
    if len(sentence_indexes) == 2:
        if arg1['sentence_index'] == sentence_indexes[-1]:
            arg1_pos[0] += len(start_sentence['text'])
            arg1_pos[1] += len(start_sentence['text'])
        if arg2['sentence_index'] == sentence_indexes[-1]:
            arg2_pos[0] += len(start_sentence['text'])
            arg2_pos[1] += len(start_sentence['text'])
    for i in range(len(words)):
        tmp_pf1.append(get_position_feature(i, arg1_pos))
        tmp_pf2.append(get_position_feature(i, arg2_pos))
    if 0 not in tmp_pf1 or 0 not in tmp_pf2 or abs(max(tmp_pf1)) > 127 or abs(min(tmp_pf1)) > 127 or abs(max(tmp_pf2)) > 127 or abs(min(tmp_pf2)) > 127:
        return [], [], []
#     print(chars)
#     print(tmp_pf1)
    pf1 = tmp_pf1
    pf2 = tmp_pf2
    return words, pf1, pf2

def init_positive_instances(docs, cropping = False):
    '''
    初始化实例，两个实体距离超过1的实例直接忽略。
    Args:
        docs: docs
    Returns:
        instances: {
            instance_id: {
                'sentence_indexs': [sentence_index1, sentence_index2],
                'chars': [chars], 
                'pos': [pos],
                'pf1': [pf1],
                'pf2': [pf2],
                'arg1_id': arg1_id,
                'arg2_id': arg2_id,
                'relation_id': relation_id,
                'relation_type': relation_type
            }
        }
    '''
    positive_instances = {}
    i = 0
    for doc_id, doc in docs.items():
        sentences = doc['sentences']
        for relation in doc['relations'].values():
            arg1, arg2 = doc['entities'][relation['arg1_id']], doc['entities'][relation['arg2_id']]
#             if abs(arg1['sentence_index'] - arg2['sentence_index']) > max_sentence_gap: continue
#             if abs(arg1['doc_position'][0] - arg2['doc_position'][0]) > max_char_gap: continue
                
            sentence_indexes = []
            start_sentnece_index = arg1['sentence_index'] if arg1['sentence_index'] <= arg2['sentence_index'] else arg2['sentence_index']
            sentence_indexes.append(start_sentnece_index)
            if arg1['sentence_index'] != arg2['sentence_index']:
                sentence_indexes.append(start_sentnece_index + 1)
            words , pf1, pf2 = get_instance_features(sentence_indexes, sentences, arg1, arg2, cropping)
            if 0 not in pf1 or 0 not in pf2: continue
            instance_id = get_instance_id_by_relation(relation)
            positive_instances[instance_id] = {
                'id': instance_id,
                'doc_id': doc_id,
                'sentence_indexs': sentence_indexes,
                'words': words, 
                'pf1': pf1,
                'pf2': pf2,
                'arg1_id': arg1['id'],
                'arg2_id': arg2['id'],
                'relation_id': relation['id'],
                'relation_type': relation['type'],
                'length_between': max(arg1['doc_position'][0], arg2['doc_position'][0]) - min(arg1['doc_position'][1], arg2['doc_position'][1]) 
            }
    
    for instance in positive_instances.values():
        instance['pf1'] = [p + 128 for p in instance['pf1']]
        instance['pf2'] = [p + 128 for p in instance['pf2']]
    return positive_instances

positive_instances = init_positive_instances(docs)