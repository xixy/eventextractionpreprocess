#coding=utf-8
import xml.etree.cElementTree as ET
import os
from pathutil import *
import re
from configure_en import *
from str_util import *
from html.parser import HTMLParser
from split_data import *
from pyltp import SentenceSplitter

from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize

import random

events_num = 0

train_sentences_num = 0
dev_sentences_num = 0
test_sentences_num = 0

train_article_num = 0
dev_article_num = 0
test_article_num = 0

convert = False

def convert(string):
	string = string.replace('``', '"')
	string = string.replace('\'\'', '"')
	return string

def convert_punctuation(line):
	'''
	将中文引号转化为英文引号
	'''
	line = line.encode('utf-8')
	line = line.replace('“', '"')
	line = line.replace('”', '"')
	line = line.replace('’', '\'')
	line = line.replace('‘', '\'')

	line = line.decode('utf-8')
	return line

def strQ2B(ustring):
	"""全角转半角"""
	rstring = ""
	for uchar in ustring:
		inside_code=ord(uchar)
		if inside_code == 12288:
		#全角空格直接转换
			inside_code = 32 
		elif (inside_code >= 65281 and inside_code <= 65374):
			#全角字符（除空格）根据关系转化
			inside_code -= 65248

		rstring += unichr(inside_code) # python2
		# rstring += chr(inside_code) # python3

	if rstring != ustring:
		print(ustring)
		print(rstring)
	return rstring

class extractor(object):
	"""从ace原始文件中提取信息"""



	def __init__(self):
		self.ace_2005_data_path = ace_2005_data_dir

		self.train_data_path = train_data_path_cn
		self.test_data_path = test_data_path_cn
		self.dev_data_path = dev_data_path_cn

		self.train_file_mapper = open(train_file_mapper_cn, 'w')
		self.test_file_mapper = open(test_file_mapper_cn, 'w')
		self.dev_file_mapper = open(dev_file_mapper_cn, 'w')

		self.file_list = []

		# train、dev、test数据集所包含的ace文件列表
		self.train_ace_file_list = []
		self.dev_ace_file_list = []
		self.test_ace_file_list = []

		# 输出文件
		self.train_output_file = open(self.train_data_path,'w')
		self.dev_output_file = open(self.dev_data_path,'w')
		self.test_output_file = open(self.test_data_path,'w')

		self.events_num = 0
		self.article_num = 0

		self.train_documents_num = 486
		self.dev_documents_sum = 0
		self.test_documents_sum = 54

	def delete_blank(self, string):
		'''
		去掉所有的空格
		'''
		if type(string) is str:
			return string.replace('\n','').replace(' ','').replace('　', '')
		return string.replace('\n','').replace(' ','').encode('utf-8').replace('　', '')

	def remove_trans(self, line):
		'''
		将XML转义字符
		'''
		return line.replace(' & ', ' &amp; ').replace('•','&#8226;')


	def parse_apf_xml(self, file_path='../data/ace_2005_td_v7/data/Chinese/bn/adj/CBS20001006.1000.0074.apf.xml'):
		'''
		从***.apf.xml文件中提取事件
		包括:anchor,extent,ldc_scope,type
		'''
		#print(file_path)

		# 事件信息
		events = []
		# 实体信息
		entities = {}
		# timex2信息
		times = {}
		# values信息
		values = {}

		tree = ET.ElementTree(file=file_path)
		root = tree.getroot()
		for elem in tree.iter(tag = 'event'):
			e_type = elem.attrib[event_type]
			e_sub_type = elem.attrib[event_sub_type]
			
			for child in elem:
				if 'event_mention' in child.tag:
					event = {}
					event[event_type] = e_type
					event[event_sub_type] = e_sub_type
					event[arguments] = []
					for item in child:
						# 提取anchor
						if anchor in item.tag:
							event[anchor] = self.remove_trans(item[0].text)
							event[anchor_offset] = [int(item[0].attrib[offset_start]),int(item[0].attrib[offset_end])]
						# 提取extent
						if extent in item.tag:
							event[extent] = self.remove_trans(item[0].text)
						# 提取ldc_scope
						if ldc_scope in item.tag:	
							event[ldc_scope] = self.remove_trans(item[0].text)
							event[ldc_scope_offset] = [int(item[0].attrib[offset_start]),int(item[0].attrib[offset_end])]
						# 提取event_mention_argument
						if event_mention_argument in item.tag:
							argument = {}
							argument[role] = item.attrib[role]
							argument[event_mention_argument] = self.remove_trans(item[0][0].text)
							argument[argument_id] = item.attrib[argument_id]
							event[arguments].append(argument)

					events.append(event)
		
		# 提取实体信息
		for elem in tree.iter(tag = 'entity'):
			e_type = elem.attrib[entity_type]
			e_sub_type = elem.attrib[entity_sub_type]
			for child in elem:
				# print child.tag
				if child.tag == 'entity_mention':
					entity = {}
					entity[entity_type] = e_type
					entity[entity_sub_type] = e_sub_type
					# entity[entity_id] = child.attrib[entity_id]
					for item in child:
						if 'extent' in item.tag:
							entity[extent] = self.remove_trans(item[0].text)
							# print(item[0].text)
							entity[extent_offset] = [int(item[0].attrib[entity_offset_start]),int(item[0].attrib[entity_offset_end])]
							# print(entity[extent_offset])
						if 'head' in item.tag:
							entity[head] = self.remove_trans(item[0].text)
							entity[head_offset] = [int(item[0].attrib[head_offset_start]),int(item[0].attrib[head_offset_end])]

					entities[child.attrib[entity_id]] = entity

		# 提取timex2
		for elem in tree.iter(tag = 'timex2'):
			for child in elem:
				time = {}
				# time[time_id] = child.attrib[time_id]
				time[extent] = self.remove_trans(child[0][0].text)
				time[time_offset] = [int(child[0][0].attrib[time_offset_start]),int(child[0][0].attrib[time_offset_end])]
				# times.append(time)
				time[time_type] = time_type_mention
				time[time_sub_type] = time_sub_type_mention
				times[child.attrib[time_id]] = time

		# 提取values
		for elem in tree.iter(tag = 'value'):
			v_type = elem.attrib[value_type]
			# 每个mention
			for child in elem:
				value = {}
				# value[value_id] = child.attrib[value_id]
				value[value_sub_type] = v_type
				value[value_type] = value_type_mention
				value[value_offset] = [int(child[0][0].attrib[entity_offset_start]),int(child[0][0].attrib[entity_offset_end])]
				value[extent] = self.remove_trans(child[0][0].text)
				# values.append(value)
				values[child.attrib[value_id]] = value

		return events, entities, times, values
	
	def construct_dataset_for_triggers(self,params):
		'''
		生成事件检测的数据集
		Args:
			params: 某文件所有段落的信息，包括[file_name, [start_idx, end_idx, param]]		
		'''

		file_name = params[0]
		# if 'CNN_ENG_20030605_193002.8' not in file_name:
		# 	return 0
		param_units = params[1:]
		events, entities, times, values = self.parse_apf_xml(file_name[:-4]+'.apf.xml')
		output_file = self.get_output_file(file_name)
		#output_file.write(file_name+'\n')
		params_labels = []
		# 以字为单位进行标记
		for param_unit in param_units:
			params_label = []
			param = param_unit[2]
			for char in param:
				params_label.append(['O','O'])
			params_labels.append(params_label)

		# 对于所有的句子都要进行处理
		# 对事件进行遍历
		for event in events:
			self.events_num += 1
			# print(event)
			# 遍历所有的事件
			event_extent = event[extent]
			anchor_start_idx, anchor_end_idx = event[anchor_offset]
			ldc_start_idx, ldc_end_idx = event[ldc_scope_offset]
			event_anchor = event[anchor]
			event_ldc_scope = event[ldc_scope]
			relative_event_start_idx = anchor_start_idx - ldc_start_idx
			relative_event_end_idx = relative_event_start_idx + len(event_anchor) - 1
			event_sub_type_ = event[event_sub_type]
			event_type_ = event[event_type]

			for param_unit,params_label in zip(param_units,params_labels):
				param_start_idx = param_unit[0]
				param_end_idx = param_unit[1]
				if param_start_idx <= anchor_start_idx and param_end_idx >= anchor_end_idx:
					#print(event_sub_type_)
					relative_idx = anchor_start_idx - param_start_idx
					for i in range(relative_idx, relative_idx + len(event_anchor)):
						params_label[i][0] = event_sub_type_
						params_label[i][1] = event_type_
						#print(params_label)
					break
		# 先检验一下
		# for param_unit, params_label in zip(param_units, params_labels):
		# 	param = param_unit[2]
		# 	#print(params_label)
		# 	for character, label in zip(param, params_label):
		# 		print(character + ' ' + label[0] + ' ' +  label[1])
		# 	print('\n')

		
		# 进行tokenize，现在拿到的只是character级别的
		# 根据空格来决定
		for param_unit,params_label in zip(param_units, params_labels):
			#print(param_unit[2])
			sentence, sub_type_labels, type_labels = [],[],[]
			word, sub_label, label = '', '', ''

			for character, types in zip(param_unit[2],params_label):
				# print(character)
				event_sub_type_, event_type_ = types[0], types[1]

				# 空格分割的词
				if character == ' ' or character == '\n' or character == '':
					if word != '':
						# 得到这个param的所有的词、标签
						sentence.append(word)
						sub_type_labels.append(sub_label)
						type_labels.append(label)
						word, sub_label, label = '', '',''
					continue

				word += str(character)
				sub_label =  event_sub_type_
				label = event_type_
				#print(word)
			
			params = []
			# 进行分词
			for word, sub_type_label, type_label in zip(sentence, sub_type_labels, type_labels):
				'''
				首先每个词进行分词
				'''
				tokens = word_tokenize(word)
				for token in tokens:
					params.append((token, sub_type_label, type_label))
			# 进行分句
			sentence = ' '.join(x[0] for x in params)
			sentences = sent_tokenize(sentence)
			sent_begin_idx = 0
			for sent in sentences:
				words = sent.split()
				_sub_types = [x[1] for x in params[sent_begin_idx:sent_begin_idx + len(words)]]
				_types = [x[2] for x in params[sent_begin_idx:sent_begin_idx + len(words)]]

				sent_begin_idx += len(words)

				# 对subtype进行BI处理
				idx = 0
				cur_type = ''
				while idx < len(_sub_types):
					if _sub_types[idx] == 'O':
						cur_type = ''
					else:
						if cur_type == '':
							cur_type = _sub_types[idx]
							_sub_types[idx] = 'B-' + _sub_types[idx]
						else:
							# 如果连续且相同，就放一块儿
							if _sub_types[idx] == cur_type:
								_sub_types[idx] = 'I-' + _sub_types[idx]
							# 防止两个挨着
							else:
								_sub_types[idx] = 'B-' + _sub_types[idx]

					idx += 1

				for word, _sub_type , _type in zip(words, _sub_types, _types):
					word = word.lower()
					output_file.write(word + ' ' + _sub_type  + ' ' + _type + '\n')
				output_file.write('\n')				


	def construct_dataset(self, params):
		'''
		生成训练集
		Args:
			params: 某文件所有段落的信息，包括[file_name, [start_idx, end_idx, param]]
		'''
		file_name = params[0]
		param_units = params[1:]

		events, entities, times, values = self.parse_apf_xml(file_name[:-4]+'.apf.xml')
		# print(entities)

		# 对事件进行遍历
		for event in events:
			# print(event)
			# 遍历所有的事件
			event_extent = event[extent]
			anchor_start_idx, anchor_end_idx = event[anchor_offset]
			ldc_start_idx, ldc_end_idx = event[ldc_scope_offset]
			event_anchor = event[anchor]
			event_ldc_scope = event[ldc_scope]
			relative_event_start_idx = anchor_start_idx - ldc_start_idx
			relative_event_end_idx = relative_event_start_idx + len(event_anchor) - 1
			event_sub_type_ = event[event_sub_type]

			# 1. 首先找出所有的arguments
			event_arguments = event[arguments]
			# print(event_arguments)
			arguments_id = [argument[argument_id] for argument in event_arguments]
			# arguments = 

			# 2. 然后找出来所有的在这个index范围内的实体
			entities_within_ldc_scope = {}
			# times_within_ldc_scope = {}
			# values_within_ldc_scope = {}

			# 2.1 找出所有的实体
			for entity_id, entity in entities.items():
				entity_start_idx, entity_end_idx = entity[extent_offset]
				if entity_start_idx >= ldc_start_idx and entity_end_idx <= ldc_end_idx:
					entities_within_ldc_scope[entity_id] = entity
			# 2.2 找出所有的时间
			for time_id, time in times.items():
				time_start_idx, time_end_idx = time[time_offset]
				if time_start_idx >= ldc_start_idx and time_end_idx <= ldc_end_idx:
					entities_within_ldc_scope[time_id] = time
			# 2.3 找出所有的数值
			for value_id, value in values.items():
				value_start_idx, value_end_idx = value[value_offset]
				if value_start_idx >= ldc_start_idx and value_end_idx <= ldc_end_idx:
					entities_within_ldc_scope[value_id] = value

			# 3. 进行标注
			event_type_ = event[entity_type]
			tokens = [x for x in event_ldc_scope] # 字的信息
			
			# 对现有的所有argument进行标注
			argument_annotations = ['O' for _ in tokens]
			for argument in event_arguments:
				arg_id = argument[argument_id]
				arg_role = argument[role]
				argument_start_idx = entities_within_ldc_scope[arg_id][extent_offset][0]
				relative_argument_start_idx = argument_start_idx - ldc_start_idx
				argument_annotations[relative_argument_start_idx] = 'B-' + arg_role
				for i in range(1, len(argument[event_mention_argument])):
					argument_annotations[relative_argument_start_idx + i] = 'I-' + arg_role

			# 遍历所有的arguments
			for argument in event_arguments:
				arg_id = argument[argument_id]
				arg_role = argument[role]
				# 生成entity向量
				entity_type_annotations = []
				# type_annotations = []
				entity_sub_type_annotations = []
				d1_annotations = []
				d2_annotations = []

				argument_start_idx = entities_within_ldc_scope[arg_id][extent_offset][0]
				relative_argument_start_idx = argument_start_idx - ldc_start_idx
				relative_argument_end_idx = relative_argument_start_idx + len(argument[event_mention_argument]) - 1

				specific_argument_annotations = []
				specific_argument_annotations.extend(argument_annotations) # 这条数据的特定的argument的标记
				# 去掉当前的argument，这是需要预测而且是未知的
				for i in range(len(argument[event_mention_argument])):
					specific_argument_annotations[relative_argument_start_idx + i] = 'O'
	
				idx = 0
				for token in tokens:
					entity_sub_type_annotations.append('O')
					entity_type_annotations.append('O')

					# 处理与触发词的相对距离
					# 如果在左边
					if idx < relative_event_start_idx:
						d1_annotations.append(idx - relative_event_start_idx)
					# 如果在右边
					elif idx > relative_event_end_idx:
						d1_annotations.append(idx - relative_event_end_idx)
					# 如果在trigger之间，那么就设为0
					else:
						d1_annotations.append(0)

					# 处理与argument candidate 的相对距离
					# 如果在左边
					if idx < relative_argument_start_idx:
						d2_annotations.append(idx - relative_argument_start_idx)
					# 如果在右边
					elif idx > relative_argument_end_idx:
						d2_annotations.append(idx - relative_argument_end_idx)
					# 如果在trigger之间，那么就设为0
					else:
						d2_annotations.append(0)
					idx += 1


					# sub_type_annotations.append('O')

				# 先处理所有的非argument的部分，然后在处理argument的部分，因为可能产生覆盖
				for entity_id, entity in entities_within_ldc_scope.items():
					if entity_id in arguments_id:
						continue
					entity_start_idx, entity_end_idx = entity[extent_offset]
					# print(entity)
					entity_sub_type_ = entity[entity_sub_type]
					entity_type_ = entity[entity_type]
					relative_start_idx = entity_start_idx - ldc_start_idx
					entity_sub_type_annotations[relative_start_idx] = 'B-' + entity_sub_type_
					entity_type_annotations[relative_start_idx] = 'B-' + entity_type_

					for i in range(1, len(entity[extent])):
						entity_sub_type_annotations[i + relative_start_idx] = 'I-' + entity_sub_type_
						entity_type_annotations[i + relative_start_idx] = 'I-' + entity_type_

				# 然后处理argument的entities
				for entity_id, entity in entities_within_ldc_scope.items():
					if entity_id not in arguments_id:
						continue
					entity_start_idx, entity_end_idx = entity[extent_offset]
					# print(entity)
					entity_sub_type_ = entity[entity_sub_type]
					entity_type_ = entity[entity_type]
					relative_start_idx = entity_start_idx - ldc_start_idx
					entity_sub_type_annotations[relative_start_idx] = 'B-' + entity_sub_type_
					entity_type_annotations[relative_start_idx] = 'B-' + entity_type_

					for i in range(1, len(entity[extent])):
						entity_sub_type_annotations[i + relative_start_idx] = 'I-' + entity_sub_type_
						entity_type_annotations[i + relative_start_idx] = 'I-' + entity_type_


				# 3.2 将\n进行处理
				idx = 0
				new_tokens = []
				new_entity_type_annotations = []
				# type_annotations = []
				new_entity_sub_type_annotations = []
				new_d1_annotations = []
				new_d2_annotations = []
				new_specific_argument_annotations = []

				new_d1 = 0
				new_d2 = 0

				# 先去掉实体的空的部分
				for token, entity_sub_type_, entity_type_, specific_argument_annotation in zip(tokens, entity_sub_type_annotations, entity_type_annotations, specific_argument_annotations):
					if token == '\n' or token == ' ':
						continue
					new_tokens.append(token)
					new_entity_type_annotations.append(entity_type_)
					new_entity_sub_type_annotations.append(entity_sub_type_)
					new_specific_argument_annotations.append(specific_argument_annotation)

				# 对d1进行修改
				idx = -1
				for token in tokens[:relative_event_start_idx][::-1]:
					if token == '\n' or token == ' ':
						continue
					new_d1_annotations.append(idx)
					idx -= 1
				new_d1_annotations = [i for i in reversed(new_d1_annotations)]
				for token in tokens[relative_event_start_idx:relative_event_end_idx + 1]:
					if token == '\n' or token == ' ':
						continue
					new_d1_annotations.append(0)					

				idx = 1
				for token in tokens[relative_event_end_idx:]:
					if token == '\n' or token == ' ':
						continue
					new_d1_annotations.append(idx)
					idx += 1

				# 对d2进行修改
				idx = -1
				for token in tokens[:relative_argument_start_idx][::-1]:
					if token == '\n' or token == ' ':
						continue
					new_d2_annotations.append(idx)
					idx -= 1	
				new_d2_annotations = [i for i in reversed(new_d2_annotations)]
				for token in tokens[relative_argument_start_idx:relative_argument_end_idx + 1]:
					if token == '\n' or token == ' ':
						continue
					new_d2_annotations.append(0)				
				idx = 1
				for token in tokens[relative_argument_end_idx:]:
					if token == '\n' or token == ' ':
						continue
					new_d2_annotations.append(idx)
					idx += 1

				# print('\n去掉了空格和换行符的结果\n')

				# 3.3 输出
				output_file = self.get_output_file(file_name)
				output_file.write(event_sub_type_ + ' ' + arg_role + ' ' + event_anchor.replace('\n','').replace(' ','') + '\n')
				for token, entity_sub_type_, entity_type_, argument_role, d1_annotation, d2_annotation, in \
				zip(new_tokens, new_entity_sub_type_annotations, new_entity_type_annotations, new_specific_argument_annotations,  new_d1_annotations, new_d2_annotations):
					output_file.write(token + ' ' + entity_sub_type_ + ' ' +  entity_type_ + ' ' + argument_role + ' ' + str(d1_annotation) + ' ' + str(d2_annotation) + '\n')

				output_file.write('\n')

	def check_offsets(self, params):
		'''
		提取实体信息，并且将实体信息与句子进行对齐，进行标记
		Args:
			params: 某文件所有段落的信息，包括[file_name, [start_idx, end_idx, param]]
		'''
		# entity_annotations = []

		file_name = params[0]
		param_units = params[1:]
		output = False

		events, entities, times, values = self.parse_apf_xml(file_name[:-4]+'.apf.xml')
		
		# 1. 检查实体信息
		for entity in entities.values():
			found = False
			# print(entity)
			extent_start_idx = entity[extent_offset][0]
			extent_end_idx = entity[extent_offset][1]
			entity_extent = entity[extent]
			# print(entity_extent)

			# 找到实体所在的段落
			for param_unit in param_units:
				param_start_idx = param_unit[0]
				param_end_idx = param_unit[1]
				if param_start_idx <= extent_start_idx and param_end_idx >= extent_end_idx:
					relative_idx = extent_start_idx - param_start_idx
					param = param_unit[2]
					param_entity = param[relative_idx:relative_idx + len(entity_extent)]

					# print(param_entity)
					if param_entity != entity_extent:
						# 加二
						if output == False:
							print(file_name)
							output = True
						print('---')
						print(entity)
						print('---')
						print(entity_extent)
						print('---')
						print(param_entity)
						print('---')
						print(param)
						print('---')

						print(param_units)
						raise Exception('出现错误')
					found = True
			if found == False:
				print(entity)
				print(param_units)
				raise Exception('存在实体没有找到对应位置')
		
		# 2. 检查事件信息
		for event in events:
			found = False
			# print(event)
			anchor_start_idx, anchor_end_idx = event[anchor_offset]

			event_anchor = event[anchor]
			for param_unit in param_units:
				param_start_idx = param_unit[0]
				param_end_idx = param_unit[1]
				# print(param_start_idx)

				if param_start_idx <= anchor_start_idx and param_end_idx >= anchor_end_idx:
					relative_idx = anchor_start_idx - param_start_idx
					param = param_unit[2]
					param_anchor = param[relative_idx:relative_idx + len(event_anchor)]

					if param_anchor != event_anchor:
						print('---')
						print(event)
						print('---')
						print(param_anchor)
						print('---')
						print(param)
						print('---')
						print(param_units)
						raise Exception('出现错误')
					found = True

			if found == False:
				print(event)
				raise Exception('存在事件没有找到对应位置')
		
		# 3. 检查时间信息
		for time in times.values():
			found = False
			# print(event)
			time_start_idx, time_end_idx = time[time_offset]

			time_extent = time[extent]
			for param_unit in param_units:
				param_start_idx = param_unit[0]
				param_end_idx = param_unit[1]
				# print(param_start_idx)

				if param_start_idx <= time_start_idx and param_end_idx >= time_end_idx:
					relative_idx = time_start_idx - param_start_idx
					param = param_unit[2]
					param_anchor = param[relative_idx:relative_idx + len(time_extent)]

					if param_anchor != time_extent:
						print('---')
						print(time)
						print('---')
						print(param_anchor)
						print('---')
						print(param)
						print('---')
						print(param_units)
						raise Exception('出现错误')
					found = True

			if found == False and time_start_idx > param_units[0][0] and len(time_extent) != len('2004-12-04T14:08:00')  \
				and len(time_extent) != len('2004-12-19T09:44:53 CST'):
				print(time)
				print(param_units)
				raise Exception('存在时间没有找到对应位置')	

		# 4. 检查数值信息
		for value in values.values():
			found = False
			# print(event)
			value_start_idx, value_end_idx = value[value_offset]

			value_extent = value[extent]
			for param_unit in param_units:
				param_start_idx = param_unit[0]
				param_end_idx = param_unit[1]
				# print(param_start_idx)

				if param_start_idx <= value_start_idx and param_end_idx >= value_end_idx:
					relative_idx = value_start_idx - param_start_idx
					param = param_unit[2]
					param_anchor = param[relative_idx:relative_idx + len(value_extent)]

					if param_anchor != value_extent:
						print('---')
						print(value)
						print('---')
						print(param_anchor)
						print('---')
						print(param)
						print('---')
						print(param_units)
						raise Exception('出现错误')
					found = True

			if found == False:
				print(value)
				raise Exception('存在数值没有找到对应位置')
		


	def extract_param(self):
		'''
		从sgm文件中提取段落文本, 每段用空格隔开，然后拼接成一行，去掉空格和URL
		'''

		count = 0
		files_params = []

		
		# 0. 先处理 bc 部分

		folder = self.ace_2005_data_path + '/' + 'data/English/bc/timex2norm/'
		sgm_files = sorted(getFilePath(folder, 'sgm'))

		for sgm_file in sgm_files:
			# if 'CNN_CF_20030304' in sgm_file:
			# 	continue
			print(sgm_file)

			start_idx, end_idx = 0, 0 #用来对段落的idx进行描述
			params = [] # 存储这个sgm文件的段落内容
			params.append(sgm_file)
			param = ''
			param_unit = [] # 存储一个param的信息
			in_param = False
			tree = ET.ElementTree(file = sgm_file)
			root = tree.getroot()
			for elem in root[:3]:
				start_idx += len(elem.text)

			# 加入HEADLINE的信息
			# 但是有的可能没有headline
			if root[3][0].tag == 'HEADLINE':
				start_idx += len(root[3][0].text) - 2

				if '\n' not in root[3][0].text[2:][:-2]:
					start_idx += 1
			#print(root[3][0].text[-4:])
			# if root[3][0].text[-4:-1] == '///':
			# 	start_idx -= 1
			# if root[3][0].text[-5:-1] == 'Zaks':
			# 	start_idx -= 1

			with open(sgm_file) as f:
				spaces = 0 # 空格的大小，在<TRUN>可能会出现
				for line in f:
					start_idx += 1
					# print(start_idx)
					# print(start_idx)
					# 开始一段正文
					if '<TURN>' in line:
						param = ''
						param_unit = []
						spaces = len(line.replace('<TEXT>', '')) - 1
						# print(spaces)
						# 计算start_idx
						param_unit.append(start_idx)
						in_param = True
						continue

					if '</TURN>' in line:
						in_param = False
						# 计算end_idx
						if param == '':
							start_idx += spaces
							spaces = 0
							continue
						param_unit.append(start_idx - 1)
						param_unit.append(param)
						params.append(param_unit)

						param = ''
						param_unit = []

					if in_param:
						# 如果是SPEAKER
						if line.startswith('<SPEAKER>'):
							#line = line[9:][:-11] + '\n'
							param_unit = []
							param_unit.append(start_idx - 1)
							start_idx += len(line[9:][:-11])
							param_unit.append(start_idx-1)
							param_unit.append(line[9:][:-11])
							params.append(param_unit)
							in_param = True

							param_unit = []
							param_unit.append(start_idx)
							continue

						param += line
						start_idx -= 1
						start_idx += len(line)
					# 处理类似于(CROSSTALK)这样的问题，背景音的描述
					if not in_param and line.startswith('('):
						start_idx += len(line)
						start_idx -= 1
			files_params.append(params)
		return files_params

		# 1. 先处理 bn 部分
		folder = self.ace_2005_data_path + '/' + 'data/English/bn/timex2norm/'
		sgm_files = getFilePath(folder, 'sgm')

		for sgm_file in sgm_files:
			#print(sgm_file)
			if 'CNN_ENG_20030616_130059.25.sgm' in sgm_file:
				continue
			# if 'CNN_ENG_20030605_193002.8' not in sgm_file:
			# 	continue
			# if 'CNR20001121.1700.1232' not in sgm_file:
			# 	continue
			start_idx, end_idx = 0, 0 #用来对段落的idx进行描述
			params = [] # 存储这个sgm文件的段落内容
			params.append(sgm_file)
			param = ''
			param_unit = [] # 存储一个param的信息
			in_param = False
			tree = ET.ElementTree(file = sgm_file)
			root = tree.getroot()
			for elem in root[:3]:
				start_idx += len(elem.text)

			with open(sgm_file) as f:
				spaces = 0 # 空格的大小，在<TRUN>可能会出现
				for line in f:
					start_idx += 1
					# print(start_idx)
					# print(start_idx)
					if '<TURN>' in line:
						param = ''
						param_unit = []
						spaces = len(line.replace('<TURN>', '')) - 1
						# print(spaces)
						# 计算start_idx
						param_unit.append(start_idx)
						in_param = True
						continue

					if '</TURN>' in line:
						in_param = False
						# 计算end_idx
						if param == '':
							start_idx += spaces
							spaces = 0
							continue
						param_unit.append(start_idx - 1)
						param_unit.append(param)
						params.append(param_unit)

						param = ''
						param_unit = []

					if in_param:

						param += line
						start_idx -= 1
						start_idx += len(line)
			files_params.append(params)
		

		# 2. 处理cts
		folder = self.ace_2005_data_path + '/' + 'data/English/cts/timex2norm/'
		sgm_files = sorted(getFilePath(folder, 'sgm'))

		for sgm_file in sgm_files:
			if 'CNN_CF_20030304' in sgm_file:
				continue
			#print(sgm_file)

			start_idx, end_idx = 0, 0 #用来对段落的idx进行描述
			params = [] # 存储这个sgm文件的段落内容
			params.append(sgm_file)
			param = ''
			param_unit = [] # 存储一个param的信息
			in_param = False
			tree = ET.ElementTree(file = sgm_file)
			root = tree.getroot()
			for elem in root[:3]:
				start_idx += len(elem.text)

			# 加入HEADLINE的信息
			# 但是有的可能没有headline
			if root[3][0].tag == 'HEADLINE':
				start_idx += len(root[3][0].text) - 2

				if '\n' not in root[3][0].text[2:][:-2]:
					start_idx += 1
			#print(root[3][0].text[-4:])
			# if root[3][0].text[-4:-1] == '///':
			# 	start_idx -= 1
			# if root[3][0].text[-5:-1] == 'Zaks':
			# 	start_idx -= 1

			with open(sgm_file) as f:
				spaces = 0 # 空格的大小，在<TRUN>可能会出现
				for line in f:
					start_idx += 1
					# print(start_idx)
					# print(start_idx)
					# 开始一段正文
					if '<TURN>' in line:
						param = ''
						param_unit = []
						spaces = len(line.replace('<TEXT>', '')) - 1
						# print(spaces)
						# 计算start_idx
						param_unit.append(start_idx)
						in_param = True
						continue

					if '</TURN>' in line:
						in_param = False
						# 计算end_idx
						if param == '':
							start_idx += spaces
							spaces = 0
							continue
						param_unit.append(start_idx - 1)
						param_unit.append(param)
						params.append(param_unit)

						param = ''
						param_unit = []

					if in_param:
						# 如果是SPEAKER
						if line.startswith('<SPEAKER>'):
							line = line[9:][:-11] + '\n'

						param += line
						start_idx -= 1
						start_idx += len(line)
					# 处理类似于(CROSSTALK)这样的问题，背景音的描述
					if not in_param and line.startswith('('):
						start_idx += len(line)
						start_idx -= 1
			files_params.append(params)
		#return files_params		
		
		

		# 3. 处理 nw 部分
		folder = self.ace_2005_data_path + '/' + 'data/English/nw/timex2norm/'
		sgm_files = sorted(getFilePath(folder, 'sgm'))

		for sgm_file in sgm_files:
			# if 'AFP_ENG_20030304.0250' in sgm_file or 'AFP_ENG_20030319.0879' in sgm_file:
			# 	continue
			start_idx, end_idx = 0, 0 #用来对段落的idx进行描述
			params = [] # 存储这个sgm文件的段落内容
			params.append(sgm_file)
			param = ''
			param_unit = [] # 存储一个param的信息
			in_param = False
			tree = ET.ElementTree(file = sgm_file)
			root = tree.getroot()
			for elem in root[:3]:
				start_idx += len(elem.text)

			# 加入HEADLINE的信息
			# 但是有的可能没有headline
			if root[3][0].tag == 'HEADLINE':
				start_idx += len(root[3][0].text) - 3

				if '\n' not in root[3][0].text[2:][:-2]:
					start_idx += 1
			#print(root[3][0].text[-4:])
			# if root[3][0].text[-4:-1] == '///':
			# 	start_idx -= 1
			# if root[3][0].text[-5:-1] == 'Zaks':
			# 	start_idx -= 1

			with open(sgm_file) as f:
				spaces = 0 # 空格的大小，在<TRUN>可能会出现
				for line in f:
					start_idx += 1
					# print(start_idx)
					# print(start_idx)
					# 开始一段正文
					if '<TEXT>' in line:
						param = ''
						param_unit = []
						spaces = len(line.replace('<TEXT>', '')) - 1
						# print(spaces)
						# 计算start_idx
						param_unit.append(start_idx)
						in_param = True
						continue

					if '</TEXT>' in line:
						in_param = False
						# 计算end_idx
						if param == '':
							start_idx += spaces
							spaces = 0
							continue
						param_unit.append(start_idx - 1)
						param_unit.append(param)
						params.append(param_unit)

						param = ''
						param_unit = []

					if in_param:

						param += line
						start_idx -= 1
						start_idx += len(line)
			files_params.append(params)


		
		# 5. 处理 wl 部分
		folder = self.ace_2005_data_path + '/' + 'data/English/wl/timex2norm/'
		sgm_files = sorted(getFilePath(folder, 'sgm'))

		for sgm_file in sgm_files:
			if 'BACONSREBELLION_20050209.0721' in sgm_file:
				continue
			if 'BACONSREBELLION_20050226.1317' in sgm_file:
				continue
			if 'FLOPPINGACES_20041114.1240.039' in sgm_file:
				continue
			if 'FLOPPINGACES_20050217.1237.014' in sgm_file:
				continue
			if 'GETTINGPOLITICAL_20050105.0127.001' in sgm_file:
				continue
			#print(sgm_file)
			start_idx, end_idx = 0, 0 #用来对段落的idx进行描述
			params = [] # 存储这个sgm文件的段落内容
			params.append(sgm_file)
			param = ''
			param_unit = [] # 存储一个param的信息
			in_param = False
			tree = ET.ElementTree(file = sgm_file)
			root = tree.getroot()
			for elem in root[:3]:
				start_idx += len(elem.text)
			# 加入HEADLINE的信息
			# print(root[3][0].text)
			# print(len(root[3][0].text))
			start_idx += len(self.remove_trans(root[3][0].text)) - 2

			if len(root[3][0].text) == 1:
				start_idx += 1

			with open(sgm_file) as f:
				spaces = 0 # 空格的大小，在<TRUN>可能会出现
				for line in f:
					start_idx += 1
					# print(start_idx)
					# print(start_idx)
					if '<POSTER>' in line:
						# print(line[8:][:-10])
						param_unit = []
						param_unit.append(start_idx - 1)


						start_idx += len(line[8:][:-10])
						param_unit.append(start_idx)
						param_unit.append(line[8:][:-10])
						params.append(param_unit)
						param_unit = []

					if '<POSTDATE>' in line:
						# print(line[10:][:-12])

						param_unit = []
						param_unit.append(start_idx - 1)
						start_idx += len(line[10:][:-12])
						param_unit.append(start_idx-1)
						param_unit.append(line[10:][:-12])
						params.append(param_unit)
						in_param = True

						param_unit = []
						param_unit.append(start_idx)
						continue
					# 结束
					if '</POST>' in line:
						in_param = False
						# 计算end_idx
						if param == '':
							start_idx += spaces
							spaces = 0
							continue
						param_unit.append(start_idx - 1)
						param_unit.append(param)
						params.append(param_unit)

						param = ''
						param_unit = []

					if in_param:
						param += line
						start_idx -= 1
						start_idx += len(line)
			files_params.append(params)
		'''

		# 4. 处理 un 部分
		folder = self.ace_2005_data_path + '/' + 'data/English/un/timex2norm/'
		sgm_files = sorted(getFilePath(folder, 'sgm'))

		for sgm_file in sgm_files:
			if 'alt.atheism_20041104.2428.sgm' in sgm_file:
				continue
			#print(sgm_file)
			start_idx, end_idx = 0, 0 #用来对段落的idx进行描述
			params = [] # 存储这个sgm文件的段落内容
			params.append(sgm_file)
			param = ''
			param_unit = [] # 存储一个param的信息
			in_param = False
			in_previous_post = False
			tree = ET.ElementTree(file = sgm_file)
			root = tree.getroot()
			for elem in root[:3]:
				start_idx += len(elem.text)
			# 加入HEADLINE的信息
			# print(root[3][0].text)
			# print(len(root[3][0].text))
			start_idx += len(self.remove_trans(root[3][0].text)) - 2

			# 

			with open(sgm_file) as f:
				spaces = 0 # 空格的大小，在<TRUN>可能会出现
				for line in f:
					start_idx += 1
					# print(start_idx)
					# print(start_idx)
					if '<POSTER>' in line:
						# print(line[8:][:-10])
						param_unit = []
						param_unit.append(start_idx - 1)

						start_idx += len(line[8:][:-10])
						param_unit.append(start_idx-1)
						param_unit.append(line[8:][:-10])
						params.append(param_unit)
						param_unit = []

					if '<POSTDATE>' in line:
						# print(line[10:][:-12])

						param_unit = []
						param_unit.append(start_idx - 1)
						start_idx += len(line[10:][:-12])
						param_unit.append(start_idx-1)
						param_unit.append(line[10:][:-12])
						params.append(param_unit)

						param_unit = []
						param_unit.append(start_idx)
						continue
					# 结束
					if '</POST>' in line:
						in_param = False
						# 计算end_idx
						if param == '':
							start_idx += spaces
							spaces = 0
							continue
						param_unit.append(start_idx - 1)
						param_unit.append(param)
						params.append(param_unit)

						param = ''
						param_unit = []

					if '<SUBJECT>' in line:
						param_unit = []
						param_unit.append(start_idx - 1)
						start_idx += len(line[9:][:-11])
						param_unit.append(start_idx-1)
						param_unit.append(line[9:][:-11])
						#print(line[9:][:-11])
						params.append(param_unit)
						in_param = True

						param_unit = []
						param_unit.append(start_idx)
						continue

					if '<QUOTE PREVIOUSPOST=' in line:
						#print(line)
						in_previous_post = True
						continue
					if '"/>' in line:
						start_idx += 1
						in_previous_post = False
						continue

					if in_previous_post:
						#print(line)
						# if line.startswith('    -'):
						# 	start_idx -= 1
						#start_idx -= 1
						continue

					if in_param:
						#print(line)
						param += line
						start_idx -= 1
						start_idx += len(line)
			files_params.append(params)
		'''

		return files_params
	def split_dataset(self, file_params):
		for file_param in file_params:
			file_name = file_param[0]
			self.file_list.append(file_name)
		print(self.file_list)
		random.shuffle(self.file_list)
		print(self.file_list)

		self.train_ace_file_list = self.file_list[:self.train_documents_num]
		# self.dev_ace_file_list = self.file_list[self.train_documents_num:self.train_documents_num + self.dev_documents_sum]

		self.test_ace_file_list = self.file_list[self.train_documents_num + self.dev_documents_sum:self.train_documents_num + self.dev_documents_sum + self.test_documents_sum]
		self.dev_ace_file_list = self.test_ace_file_list






	def get_output_file(self, line):
		'''
		根据文件列表名来判断应该输出到train、dev、test哪个文件
		Args:
			line:../../data/ace_2005_td_v7/data/Chinese/bn/adj/CTS20001218.1300.0965.sgm
		'''
		# name = line.split('/')[-1][:-4]
		name = line
		#print(self.train_ace_file_list)
		# print(line)
		# print(name)
		if name in self.train_ace_file_list:
			return self.train_output_file
		elif name in self.test_ace_file_list:
			return self.test_output_file
		elif name in self.dev_ace_file_list:
			return self.dev_output_file
		else:
			# print(line)
			print(name)
			# return self.train_output_file
			raise Exception('找不到归类')
					

if __name__ == '__main__':
	#global events_num
	ext = extractor()
	# 从sgm文件中提取段落
	file_params = ext.extract_param()
	# 对文件进行遍历
	ext.split_dataset(file_params)

	for file_param in file_params:
		ext.check_offsets(file_param)
		ext.construct_dataset_for_triggers(file_param)
		ext.article_num += 1
		# break
	# print(ext.parse_apf_xml())
	# ext.create_sequence_tag()
	ext.train_output_file.close()
	ext.dev_output_file.close()
	ext.test_output_file.close()

	
	print('事件个数为'+str(ext.events_num))
	print('文件数量为'+str(ext.article_num))





		