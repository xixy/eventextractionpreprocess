#coding=utf-8
import xml.etree.cElementTree as ET
import os
from pathutil import *
import re
from configure import *
from str_util import *
from html.parser import HTMLParser
from split_data import *

events_number_1 = 0
events_number_2 = 0
events_number_3 = 0

train_sentences_num = 0
dev_sentences_num = 0
test_sentences_num = 0

train_article_num = 0
dev_article_num = 0
test_article_num = 0

convert = False

def convert_punctuation(line):
	'''
	将中文引号转化为英文引号
	'''
	line = line.encode('utf-8')
	line = line.replace('“', '"')
	line = line.replace('”', '"')
	line = line.replace('’', '\'')
	line = line.replace('‘', '\'')

	line = line.decode('utf-8')
	return line

def strQ2B(ustring):
	"""全角转半角"""
	rstring = ""
	for uchar in ustring:
		inside_code=ord(uchar)
		if inside_code == 12288:
		#全角空格直接转换
			inside_code = 32 
		elif (inside_code >= 65281 and inside_code <= 65374):
			#全角字符（除空格）根据关系转化
			inside_code -= 65248

		rstring += unichr(inside_code) # python2
		# rstring += chr(inside_code) # python3

	if rstring != ustring:
		print(ustring)
		print(rstring)
	return rstring

class extractor(object):
	"""从ace原始文件中提取信息"""

	def __init__(self):
		self.ace_2005_data_path = ace_2005_data_dir
		self.middle_file_path = './middle_result.dat'
		self.param_as_sentence_path = './param_as_sentence.dat'
		self.sentences_path = './sentences.txt'



		self.train_data_path = './train_data.txt'
		self.test_data_path = './test_data.txt'
		self.dev_data_path = './dev_data.txt'

		self.train_file_mapper = open('./train_file_mapper.txt', 'w')
		self.test_file_mapper = open('./test_file_mapper.txt', 'w')
		self.dev_file_mapper = open('./dev_file_mapper.txt', 'w')

		# train、dev、test数据集所包含的ace文件列表
		self.train_ace_file_list = getFilesForDataSets(ace_npn_train_data_path)
		self.dev_ace_file_list = getFilesForDataSets(ace_npn_dev_data_path)
		self.test_ace_file_list = getFilesForDataSets(ace_npn_test_data_path)

		# 输出文件
		self.train_output_file = open(self.train_data_path,'w')
		self.dev_output_file = open(self.dev_data_path,'w')
		self.test_output_file = open(self.test_data_path,'w')

	def delete_blank(self, string):
		'''
		去掉所有的空格
		'''
		new_string = string.replace(' \n', ' ')
		new_string = new_string.replace('\n', ' ')
		return new_string
		# return string.replace('\n',' ')
		# if type(string) is str:
		# 	return string.replace('\n','').replace(' ','').replace('　', '')
		# return string.replace('\n','').replace(' ','').encode('utf-8').replace('　', '')

	def parse_apf_xml(self, file_path='../../data/ace_2005_td_v7/data/Chinese/bn/adj/CBS20001006.1000.0074.apf.xml'):
		'''
		从***.apf.xml文件中提取事件
		包括:anchor,extent,ldc_scope,type
		'''
		events = []

		tree = ET.ElementTree(file=file_path)
		root = tree.getroot()
		for elem in tree.iter(tag = 'event'):
			e_type = elem.attrib[event_type]
			e_sub_type = elem.attrib[event_sub_type]
			
			for child in elem:
				if 'event_mention' in child.tag:
					event = {}
					event[event_type] = e_type
					event[event_sub_type] = e_sub_type
					event[arguments] = []
					for item in child:
						# 提取anchor
						if anchor in item.tag:
							event[anchor] = self.delete_blank(item[0].text)
							event[anchor_offset] = [int(item[0].attrib[offset_start]),int(item[0].attrib[offset_end])]
						# 提取extent
						if extent in item.tag:
							event[extent] = self.delete_blank(item[0].text)
						# 提取ldc_scope
						if ldc_scope in item.tag:	
							event[ldc_scope] = self.delete_blank(item[0].text)
							event[ldc_scope_offset] = [int(item[0].attrib[offset_start]),int(item[0].attrib[offset_end])]
						# 提取event_mention_argument
						if event_mention_argument in item.tag:
							argument = {}
							argument[role] = item.attrib[role].encode('utf-8')
							argument[event_mention_argument] = self.delete_blank(item[0][0].text)
							event[arguments].append(argument)

					events.append(event)
		return events

	def extract_param(self):
		'''
		从sgm文件中提取段落文本, 每段用空格隔开，然后拼接成一行，去掉空格和URL
		'''

		# 首先从sgm文件中提取文本，存入到middle_result.dat文件中

		middle_file = open(self.middle_file_path, 'w')
		count = 0

		# 先处理 bn 部分
		folder = self.ace_2005_data_path + '/' + 'data/English/bn/timex2norm/'
		# folder = self.ace_2005_data_path + '/' + 'data/Chinese/bn/adj/'
		sgm_files = getFilePath(folder, 'sgm')

		for sgm_file in sgm_files:
			print(sgm_file)
			count += 1
			param = ''
			middle_file.write(sgm_file)
			tree = ET.ElementTree(file = sgm_file)
			root = tree.getroot()
			# text=open(sgm_file).read()
			# text=re.sub(u"[\x00-\x08\x0b-\x0c\x0e-\x1f]+",u"",text)
			# root=ET.ElementTree.fromstring(text)
			for elem in root[3]:
				for turn in elem:
					param += turn.text
			# print(param)
			# param = param.replace(' ', '')
			# param =
			middle_file.write(param.encode('utf-8'))
			# middle_file.write(param)

		# 处理nw部分
		# folder = self.ace_2005_data_path + '/' + 'data/Chinese/nw/adj/'
		folder = self.ace_2005_data_path + '/' + 'data/English/nw/timex2norm/'
		sgm_files = getFilePath(folder, 'sgm')

		for sgm_file in sgm_files:
			print(sgm_file)
			count += 1
			param = ''
			middle_file.write(sgm_file)
			tree = ET.ElementTree(file = sgm_file)
			root = tree.getroot()
			# print(root[3][0].text)

			print(len(root[3]))
			if len(root[3]) < 2:
				param = root[3][0].text
			else:
				param = root[3][1].text

			# param = text.replace(' ', '')
			middle_file.write(param.encode('utf-8'))
			# middle_file.write(param)

		# 处理wl部分, 需要用文本的方法进行处理，没法用xml解析
		# folder = self.ace_2005_data_path + '/' + 'data/Chinese/wl/adj/'
		folder = self.ace_2005_data_path + '/' + 'data/English/wl/timex2norm/'
		sgm_files = getFilePath(folder, 'sgm')
		for sgm_file in sgm_files:
			print(sgm_file)
			count += 1
			middle_file.write(sgm_file + '\n')
			param = ''
			InText = False
			for line in open(sgm_file):
				if '</POST>' in line:
					InText = False
				if InText:
					param += line
				if '<POSTDATE>' in line:
					InText = True
					param += '\r\n'
			# param.replace(' ', '')
			middle_file.write(param)

		# 处理bc部分
		# folder = self.ace_2005_data_path + '/' + 'data/Chinese/wl/adj/'
		folder = self.ace_2005_data_path + '/' + 'data/English/bc/timex2norm/'
		sgm_files = getFilePath(folder, 'sgm')
		for sgm_file in sgm_files:
			print(sgm_file)
			count += 1
			middle_file.write(sgm_file + '\n')
			param = ''
			InText = False
			for line in open(sgm_file):
				if '</TURN>' in line:
					InText = False
				if InText:
					param += line
				if '<SPEAKER>' in line:
					InText = True
					param += '\r\n'
			# param.replace(' ', '')
			middle_file.write(param)

		# 处理cts部分
		# folder = self.ace_2005_data_path + '/' + 'data/Chinese/wl/adj/'
		folder = self.ace_2005_data_path + '/' + 'data/English/cts/timex2norm/'
		sgm_files = getFilePath(folder, 'sgm')
		for sgm_file in sgm_files:
			print(sgm_file)
			count += 1
			middle_file.write(sgm_file + '\n')
			param = ''
			InText = False
			for line in open(sgm_file):
				if '</TURN>' in line:
					InText = False
				if InText:
					param += line
				if '<SPEAKER>' in line:
					InText = True
					param += '\r\n'
			# param.replace(' ', '')
			middle_file.write(param)

		# 处理un部分
		# folder = self.ace_2005_data_path + '/' + 'data/Chinese/wl/adj/'
		folder = self.ace_2005_data_path + '/' + 'data/English/un/timex2norm/'
		sgm_files = getFilePath(folder, 'sgm')
		for sgm_file in sgm_files:
			print(sgm_file)
			count += 1
			middle_file.write(sgm_file + '\n')
			param = ''
			InText = False
			for line in open(sgm_file):
				if '</POST>' in line or '<QUOTE' in line:
					InText = False
				if InText:
					param += line
				if '\"/>' in line or '<SUBJECT>' in line:
					InText = True
					param += '\r\n'
					if '<SUBJECT>' in line:
						param += line[10:][:-11]
						param += '\r\n'
			# param.replace(' ', '')
			middle_file.write(param)
		print(count)

		middle_file.close()

		# 从middle_result.dat中读取刚才生成的中间结果，拼接成一个段落一句
		data = [[]]
		param = ''
		current_file = None
		index = 0
		count = 0
		for line in open(self.middle_file_path, 'r'):
			# line = line.strip() + ' '
			# line = line.strip()
			line = line.replace('\n', '')
			print(line)

			# 如果是文件列表行
			if '../data' in line:
				count += 1
				
				# output_file.write(line + '\n')

				# 如果是前面的结束，把这段加进去
				if param != '':
					data[index].append(param)
					param = ''
				index += 1
				data.append([])
				current_file = line
				
				data[index].append(line)
				# print(current_file)
				continue

			# 如果是空行，一段结束, 就把这个段落加进去
			if len(line) < 1:
				if param != '':
					# 去掉空格
					# param.replace(' ','')
					param.replace('\n', '')
					data[index].append(param)
					param = ''
			# 否则就把这行并起来
			else:
				if line[-1] != ' ':
					line = line + ' '
				# line = line + ' '
				print(line)
				# 如果是URL行，就扔掉
				if not self.is_URL(line):
					param += line

		print('处理得到'+str(count)+'个文件')

		# 结果写入文件
		output_file = open(self.param_as_sentence_path,'w')

		index = 0
		# print len(data)
		for files in data:
			if len(files) < 1:
				continue
			# print files
			index += 1
			file_path = files[0]
			output_file.write(file_path + '\n')
			for param in files[1:]:
				output_file.write(param + '\n') 
		# for file_path in data:
		# 	output_file.write(file_path + '\n')
		# 	for param in data[file_path]:
		# 		output_file.write(param + '\n') 
		output_file.close()
		print('处理得到'+str(index)+'个文件')



	def is_URL(self, text):
		'''
		判断是否URL
		'''
		if 'http://' in text and '.html' in text:
			return True
		return False

	def printlist(self, sentences_with_events):
		print('---------------打印-----------')
		for scope, event in sentences_with_events:
			print(scope)
			for item in event:
				print(item[ldc_scope],item[anchor],item[anchor_offset][0])

	def find_indexes(self, indexs, begin, article, event):
		'''
		查找该事件对应的几个句子，返回首尾的index
		Args:
			indexs:[[0,10],[11,16]...]
			begin: 表示在文章中查找的起始位置
			article:表示句子

		Return:
			start_idx:事件对应的起始句子idx
			end_idx:事件对应的结束句子idx
			start:表示ldc_scope在段落中的起始位置
			end: 表示ldc_scope在段落中的结束位置
		'''
		# 先找到范围
		start = article.index(event[ldc_scope],begin)
		# print(start)
		end = start + len(event[ldc_scope]) - 1
		# print(end)
		start_idx = None # 文章中开始的句子id
		end_idx = None # 文章中结束的句子id
		# print(indexs)
		
		for idx in indexs:
			# 先判断起始位置
			if start_idx == None and idx[0] <= start:
				# 如果都小于开头，那就不在当前句子
				if idx[1] < start:
					continue
				# 如果结尾大于开头，那开始部分就是当前句子
				else:
					start_idx = indexs.index(idx)
			# 然后判断终止位置
			if idx[1] >= end:
				end_idx = indexs.index(idx)
				break
		return start, end, start_idx, end_idx

	def map_events_articles(self, events, lines, article, file_path):
		'''
		将events和文章对应起来，这里有问题，事件少了，返回的只有3303个事件
		Args:
			events:[event]
			lines: [line]每行是一个分出来的句子
			article: 一篇文章拼接起来的string
			file_path: 该文章所在的路径
		'''
		# print '事件数量为%d' % len(events)


		# 1. 首先统计句子的边界信息
		sentences_with_events = [] # 表示scope和事件信息 [[[scope1],[event1, event2, event3...], ...]]
		count = 0
		indexs = [] # 每个句子的起始index和结尾index [(0, 10),(11, 15),...]
		for sentence in lines:
			indexs.append([count, count + len(sentence) - 1])
			count += len(sentence)
		# print(count)


		# 2. 针对事件信息来进行对齐
		for event in events:
			# print(event)


			# 如果ldc_scope不在句子中，那么就报错
			if event[ldc_scope] not in article or event[extent] not in article:
				print(event[ldc_scope])
				print('++++++++++++++++++++++')
				print(event[extent])
				print('++++++++++++++++++++++')
				print(article)
				raise Exception('出现了找不到的文本')
			# 现在进行处理
			begin_idx = 0
			find_right_index = False

			while find_right_index == False:
				# start_idx, end_idx表示句子id
				# start, end表示ldc_scope在article中的index
				start, end, start_idx, end_idx = self.find_indexes(indexs, begin_idx, article, event) # 找到indexes

				# 将事件对应的句子进行记录
				scopes = [i for i in range(start_idx, end_idx + 1)]

				# 将scope和event进行存储
				index = 0
				for item in sentences_with_events:
					# 如果该scopes已经存在，那么就放进去
					if item[0] == scopes:
						break
					index += 1

				# 如果scopes已经存在
				if index < len(sentences_with_events):
					same_exist = False
					# 如果不相同，就直接extend

					anchor_idx = 0

					# 找个这个scopes中的事件
					for former_event in sentences_with_events[index][1]:
						# 如果事件基本相同
						if former_event[anchor] == event[anchor] and former_event[ldc_scope] == event[ldc_scope] and former_event[ldc_scope_offset] != event[ldc_scope_offset]:
							# print '相同存在'
							# print former_event[anchor],former_event[ldc_scope]
							# print event[anchor], event[ldc_scope]
							same_exist = True
							break

					# 如果没有相同的事件，那就直接加进去
					if same_exist == False:
						sentences_with_events[index][1].extend([event])
						find_right_index = True
					# 如果相同，那么就要继续往下找
					else:
						begin_idx = end

				# 如果scope不存在，就直接新建
				else:
					sentences_with_events.append([scopes, [event]])
					find_right_index = True

			# if len(scopes) < 2:
			# 	continue
			# print 'ldc_scope------------------------------111111111111-------'
			# print event[ldc_scope]
			
			# print 'mapped sentences-------------------------000000000000------'
			# for scope in scopes:
			# 	print '[' + str(scope)+ ']' + lines[scope]

		# self.printlist(sentences_with_events)

		# 3. 处理统计到的事件中scope可能的重叠
		scopes_with_events = {}
		events_loaded = [] # 已经被归并的事件
		flag = True # 如果一次循环没有发生变化，那就是合并结束了
		# overlapped = True 

		# 只要发生一次合并，就循环
		while flag == True:
			# self.printlist(sentences_with_events)

			flag = False
			scopes_with_events = []
			events_loaded = []

			# 对事件两两遍历
			i = 0
			while i < len(sentences_with_events):
				overlapped = False
				scope1, events1 = sentences_with_events[i]
				if events1 in events_loaded:
					i += 1
					continue

				j = i + 1
				while j < len(sentences_with_events):
					scope2, events2 = sentences_with_events[j]
					if events2 in events_loaded:
						j += 1
						continue
					# 如果有重叠
					if intersect(scope1, scope2) != []:
						# print scope1
						# print scope2
						# print str(events1).decode("string_escape")
						# print str(events2).decode("string_escape")
						flag = True # 发生了重叠
						new_scope = union(scope1, scope2)
						new_events = events1 + events2
						# set(events1) + events2
						scopes_with_events.append([new_scope, new_events])
						events_loaded.append(events1)
						events_loaded.append(events2)

						overlapped = True
					j += 1

				# 如果没有重叠
				if overlapped == False:
					scopes_with_events.append([scope1, events1])
					events_loaded.append(events1)
				i += 1

			# 更新
			sentences_with_events = scopes_with_events
			# self.printlist(sentences_with_events)


		# 4. 删掉重复的
		idx = 0
		for scopes, not_pure_events in sentences_with_events:
			pure_events = []
			for event in not_pure_events:
				if event not in pure_events:
					pure_events.append(event)
			sentences_with_events[idx][1]= pure_events
			idx += 1
				# while i < len(pure_events):
				# 	pure_event = pure_events[i]


					# if former_event[anchor] == event[anchor] and former_event[ldc_scope] == event[ldc_scope] and former_event[ldc_scope_offset] != event[ldc_scope_offset]:



		# self.printlist(sentences_with_events)
		# 5. 进行检查，判断是否所有的事件都进行了处理
		events_coped = []
		for _, pure_events in sentences_with_events:
			events_coped.extend(pure_events)

		for event in events:
			if event not in events_coped:
				raise Exception('事件丢失')

		global events_number_1
		events_number_1 += len(events_coped)

		ev_num_1 = len(events)
		ev_num_2 = 0
		for scopes, event in sentences_with_events:
			ev_num_2 += len(event)

		# 判断是否相等
		# assert ev_num_2 == ev_num_1, file_path
		if ev_num_1 != ev_num_2:
			print('------------------384行报错---------------')
			print(file_path)
			print(str(ev_num_2)+str(len(events)))
			for event in events:
				print(event[anchor]+event[ldc_scope])

			# print file_path

			self.printlist(sentences_with_events)

		# 对结果进行输出
		return sentences_with_events




	def write_sequence_tag(self, sentences_with_events, output_file, lines, file_path):
		'''
		对结果进行输出
		'''
		global train_sentences_num 
		global dev_sentences_num
		global test_sentences_num

		global train_article_num
		global dev_article_num
		global test_article_num

		data_file_mapper = None

		if output_file == self.train_output_file:
			train_sentences_num += len(lines)
			train_article_num += 1
			data_file_mapper = self.train_file_mapper
		elif output_file == self.dev_output_file:
			dev_sentences_num += len(lines)
			dev_article_num += 1
			data_file_mapper = self.dev_file_mapper
		else:
			test_sentences_num += len(lines)
			test_article_num += 1
			data_file_mapper = self.test_file_mapper


		# 先输出有事件的句子，并且统计有事件的句子
		global events_number_2
		global events_number_3
		# output_file.write(str(len(lines)))

		if len(lines) < 1:
			raise Exception('文章为空')
		scopes_with_events = []# 包含了事件的句子id

		index_used = []
		events_added = []

		for scopes, events in sentences_with_events:
			# print(events)
			index_used = []
			events_added = []

			ev_num_1 = 0
			ev_num_2 = 0

			scopes_with_events.extend(scopes)# 对有事件的句子统计
			line = ''.join([lines[i] for i in scopes])
			line = line.decode('utf-8')
			anchor_indexs = []
			# 找到anchor的index
			for event in events:
				my_buffer_scope = buffer_scope
				# 先找ldc_scope的index，然后用anchor的offset计算即可

				# 先找ldc_scope的index，这是唯一的
				ldc_scope_index = line.index(event[ldc_scope].decode('utf-8'))
				# 然后找anchor的index，但是可能offset信息不准，有空格和换行的存在，所以只能通过这个来算一个大概范围


				not_found = True
				while not_found:
					anchor_index_begin = ldc_scope_index + event[anchor_offset][0] - event[ldc_scope_offset][0] - my_buffer_scope
					if anchor_index_begin < 0:
						anchor_index_begin = 0

					try:
						# 然后从这个位置开始找第一个，应该是对的？
						anchor_index = line.index(event[anchor].decode('utf-8'),anchor_index_begin)
						if anchor_index in index_used:
							my_buffer_scope -= 1

							# print '找到了重复的'
							# print '有效句子距离为:' + line[anchor_index_begin:].encode('utf-8')
							# print str(event).decode('string_escape')
							# print '已占位的事件为:' + str(events_added[index_used.index(anchor_index)]).decode('string_escape')
							# print '\n'
							continue

						# if line[anchor_index:anchor_index + len(event[anchor].decode('utf-8'))]
						not_found = False
					except Exception:
						not_found = True
						my_buffer_scope += 1
						# print line.encode('utf-8')
						# print 'anchor:' + event[anchor]
						# print anchor_index_begin
						# print '截断句子:' + line[anchor_index_begin:].encode('utf-8')
						# print event[ldc_scope]
						# # print event[anchor_offset]
						# raise Exception('找不到对应的index')
				# 需要判断对不对
				# print '------------------------------------'
				# print 'anchor:' + event[anchor]
				# # print anchor_index
				# # print anchor_index_begin
				# print '句子:' + line.encode('utf-8')
				# print '有效句子:' + line[anchor_index:].encode('utf-8')
				# print 'ldc_scope:' + event[ldc_scope]

				anchor_indexs.append([anchor_index, event])
				index_used.append(anchor_index)
				events_added.append(event)

			# 对该socpes进行输出
			events_number_2 += len(anchor_indexs)
			tagged_scope = []

			# 先全标为O
			for char in line.split(' '):
				tagged_scope.append([char, 'O',None])

			# 然后标记BI结果
			for anchor_index, event in anchor_indexs:

				ev_num_1 += 1
				
				# 计算BIO
				if tagged_scope[anchor_index][1][:2] == 'B-':
					print('重复标记了521')
					print(str(event).decode('string_escape'))
					print(tagged_scope[anchor_index-1][0].encode('utf-8'))
					print(tagged_scope[anchor_index][1])
					print('已标记的事件:' + str(tagged_scope[anchor_index][2]).decode('string_escape'))
					print(line.encode('utf-8'))
					print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
					ev_num_2 += 1

				tagged_scope[anchor_index][1] = 'B-' + event[event_sub_type]
				tagged_scope[anchor_index][2] = event

				for i in range(anchor_index + 1, anchor_index + len(event[anchor].decode('utf-8'))):
					if tagged_scope[i][1][:2] == 'B-':

						# continue
						print('重复了')
						print(line.encode('utf-8'),line[i+1].encode('utf-8'),tagged_scope[i][1], event[anchor])
					tagged_scope[i][1] = 'I-' + event[event_sub_type]
					tagged_scope[i][2] = event

			# 进行输出
			for char, tag, event in tagged_scope:
				if tag[:2] == 'B-':
					ev_num_2 += 1
					events_number_3 += 1
				if convert:
					char = strQ2B(char)
					char = convert_punctuation(char)
				output_file.write(char.encode('utf-8') + ' '+ tag + '\n')

				# # 记录结果
				# if output_file == self.train_output_file:
				# 	train_sentences.append(train_sentence_index)
				# else:
				# 	test_sentences.append(train_sentence_index)
				# if output_file != self.test_output_file:
				# 	train_sentence_index += 1


			output_file.write(file_path + '\n')
			data_file_mapper.write(file_path + '\n')

			if ev_num_1 != ev_num_2:
				print('+++++++++++++++++++++++++++++++++++++++++++++++++++')
				print(str(ev_num_1)+str(ev_num_2))
				# 标记结果
				print('标记结果为')
				for char, tag, event in tagged_scope:
					print(char.encode('utf-8') + ' '+ tag)
				# 事件数据
				print('事件数据为')
				for anchor_index, event in anchor_indexs:
					print(event[anchor]+event[ldc_scope])
				print(tagged_scope)



		# 然后输出没有事件的句子
		if output_file == self.dev_output_file or output_file  == self.test_output_file:
			if remove_empty_instance:
				return
		for i in range(len(lines)):
			if i not in scopes_with_events:
				for char in lines[i].decode('utf-8'):
					if convert:
						char = strQ2B(char)
						char = convert_punctuation(char)
					output_file.write(char.encode('utf-8') + ' O' + '\n')

				output_file.write(file_path + '\n')
				data_file_mapper.write(file_path + '\n')


	def get_output_file(self, line):
		'''
		根据文件列表名来判断应该输出到train、dev、test哪个文件
		Args:
			line:../../data/ace_2005_td_v7/data/Chinese/bn/adj/CTS20001218.1300.0965.sgm
		'''
		name = line[-21:]
		name = line.split('/')[-1]
		# print(name)
		if name in self.train_ace_file_list:
			return self.train_output_file
		elif name in self.test_ace_file_list:
			return self.test_output_file
		elif name in self.dev_ace_file_list:
			return self.dev_output_file
		else:
			print(line)
			print(name)
			return self.train_output_file
			# raise Exception('找不到归类')

	def create_sequence_tag(self):
		'''
		根据分句之后的结果，将句子进行标记，以字为单位
		'''
		# html_parser = HTMLParser.HTMLParser()
		html_parser = HTMLParser()
		# output_file = open(self.sequence_tag_path,'w')
		events = None # 从apf文件中提取的事件信息
		file_path = None # 表示事件的文件
		lines = [] # 表示句子的集合
		article = '' # 表示文章的str

		# 对结果进行统计
		events_number = 0
		article_number = 0
		lines_number = 0


		with open(self.sentences_path) as f:
			for line in f:
				print(line)

				# line = line.strip()
				line = line.replace('\r\n', '')
				# 如果是文件列表行
				if '../data' in line:
					# 如果上一篇文章处理完毕，接下来进行事件的对应
					if len(lines) > 0:
						article_number += 1
						output_file = self.get_output_file(file_path)
						sentences_with_events = self.map_events_articles(events, lines, article, file_path)
						self.write_sequence_tag(sentences_with_events, output_file, lines, file_path)


					article = ''
					lines = []
					# 提取这个文件对应的apf文件中的事件
					file_path = line[:-4]
					events = self.parse_apf_xml(line[:-4]+'.apf.xml')
					events_number += len(events)
					

				# 如果不是的话
				else:
					# print(line)
					line = line.replace('\n', '')
					print(line)
					line = line.decode('utf-8')
					line = html_parser.unescape(line)
					line = line.encode('utf-8')
					line = self.delete_blank(line)
					# print(line)

					
					
					# if line[-1] != ' ':
					# 	line += ' '
					line += ' '
					print(line)
					article += line
					lines.append(line)
					# print(line)

					# article += ' '
					lines_number += 1

		# 处理剩下的一个文章
		if len(lines) > 0:
			article_number += 1
			output_file = self.get_output_file(file_path)
			sentences_with_events = self.map_events_articles(events, lines, article, file_path)
			self.write_sequence_tag(sentences_with_events, output_file, lines, file_path)

		self.train_output_file.close()
		self.dev_output_file.close()
		self.test_output_file.close()

		self.train_file_mapper.close()
		self.test_file_mapper.close()
		self.dev_file_mapper.close()
		# 对统计结果进行输出
		global events_number_1
		global events_number_3
		print('解析结束，总共解析到' + str(events_number) + '个事件')
		print('总共有' + str(article_number) + '篇文章')
		print('总共有' + str(lines_number) + '句话')
		print('train set包括ACE中' + str(len(self.train_ace_file_list)) + '个文件')
		print('dev set包括ACE中' + str(len(self.dev_ace_file_list)) + '个文件')
		print ('test set包括ACE中' + str(len(self.test_ace_file_list)) + '个文件')

		print ('第一步结束总共得到' + str(events_number_1) + '个事件')
		print ('第二步结束总共得到' + str(events_number_2) + '个事件')
		print ('第三步结束总共得到' + str(events_number_3) + '个事件')

		global train_sentences_num 
		global dev_sentences_num
		global test_sentences_num

		global train_article_num
		global dev_article_num
		global test_article_num

		print('train set 包括文章个数、句子个数为:' + str(train_sentences_num) + '、'+str(train_article_num))
		print('dev set 包括文章个数、句子个数为:' + str(dev_sentences_num) + '、'+ str(dev_article_num))
		print('test set 包括文章个数、句子个数为:' + str(test_sentences_num) + '、'+ str(test_article_num))

		# global train_sentences# 用来记录train_sentence的id
		# global test_sentences # 用来记录test_sentence的id
		# global train_sentence_index
		# print('train set总共'+str(train_sentence_index)+'个句子')
					

if __name__ == '__main__':
	ext = extractor()
	# 从sgm文件中提取段落
	# ext.extract_param()
	# print ext.parse_apf_xml()
	ext.create_sequence_tag()





		