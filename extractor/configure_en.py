#coding=utf-8

anchor = 'anchor'
extent = 'extent'
ldc_scope = 'ldc_scope'
event_type = 'TYPE'
event_sub_type = 'SUBTYPE'
event_mention_argument = 'event_mention_argument'
role = 'ROLE'
arguments = 'arguments'
ldc_scope_offset = 'ldc_scope_offset'
offset_start = 'START'
offset_end = 'END'
anchor_offset = 'anchor_offset'
buffer_scope = 6 # 表示在找index的时候对offset的容忍度
remove_empty_instance = False
argument_id = 'REFID'


data_dir = '../data/'
ace_2005_data_dir = data_dir + 'ace_2005_td_v7'
ace_npn_data_dir = data_dir + 'ace_data'
ace_npn_train_data_path = ace_npn_data_dir + '/train/train.word.dat'
ace_npn_test_data_path = ace_npn_data_dir + '/test/test.word.dat'
ace_npn_dev_data_path = ace_npn_data_dir + '/dev/dev.word.dat'


train_data_path_cn = './train.data.en.txt'
test_data_path_cn = './test.data.en.txt'
dev_data_path_cn = './dev.data.en.txt'

train_file_mapper_cn = './train.mapper.en.txt'
test_file_mapper_cn = './test.mapper.en.txt'
dev_file_mapper_cn = './dev.mapper.en.txt'


# entity-related parameters
entity_type = 'TYPE'
entity_sub_type = 'SUBTYPE'
entity_offset_start = 'START'
entity_offset_end = 'END'
extent_offset = 'extent_offset'
entity_id = 'ID'
head = 'head'
head_offset_start = 'START'
head_offset_end = 'END'
head_offset = 'head_offset'

time_id = 'ID'
time_offset_start = 'START'
time_offset_end = 'END'
time_offset = 'extent_offset'
time_type = 'TYPE'
time_sub_type = 'SUBTYPE'
time_type_mention = 'TIME'
time_sub_type_mention = 'Time'

value_type = 'TYPE'
value_sub_type = 'SUBTYPE'
value_type_mention = 'VALUE'
value_offset_start = 'START'
value_offset_end = 'END'
value_offset = 'extent_offset'
value_id = 'ID'

