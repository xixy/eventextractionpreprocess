#coding=utf-8
from configure import *

from word_util import *

def main():
	'''
	加入词的信息，包括word ids、seg ids
	'''
	# 处理测试集
	add_word_info(test_data_id_path, word_vocab_path, test_data_id_lm_path, full_test_data_id_lm_path, test_data_seg_path, seg_mode = seg_mode)
	print('test处理结束')
	# 处理训练集
	add_word_info(train_data_id_path, word_vocab_path, train_data_id_lm_path, full_train_data_id_lm_path, train_data_seg_path, seg_mode = seg_mode)
	print('train处理结束')
	# 处理dev集
	add_word_info(dev_data_id_path, word_vocab_path, dev_data_id_lm_path, full_dev_data_id_lm_path, dev_data_seq_path, seg_mode = seg_mode)
	print('dev处理结束')

	# add_sense_info(test_data_id_path, word_vocab_path, full_test_data_id_lm_path, full_test_data_id_lm_sense_path)





if __name__ == '__main__':
	main()