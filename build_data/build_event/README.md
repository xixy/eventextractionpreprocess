数据处理，包括四步：
1. make vocab 处理生成tag、字、词的vocab文件
2. make data 将数据集进行id表示，并且加入语言模型向量
3. make word 将分词信息引入进来
处理得到的格式是五列：
字id 标注 语言模型向量 字的seg 词id
4. make mtl 引入多任务标签，的到的格式是7列

字-id 67-tag-id 17-tag-id 3-tag-id 语言模型向量 字-seg-id 词-id