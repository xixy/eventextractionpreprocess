#coding=utf-8
from configure import *
from event_types import *
from vocab import *
from dataset import *
from mapping_17 import *

# 构造17-BIO voab
tag_17_vocab = []
for higher_type in event_types:
	tag_17_vocab.append('B-'+higher_type)
	tag_17_vocab.append('I-'+higher_type)
tag_17_vocab.append('O')
tag_17_vocab.sort()
write_vocab(tag_17_vocab, tag_17_voacb_path)

# 构造3-BIO vocab
tag_3_vocab = ['B', 'I', 'O']
write_vocab(tag_3_vocab, tag_3_voacb_path)

# tag_17_voacb = load_vocab(tag_17_voacb_path)
tag_3_voacb = load_vocab(tag_3_voacb_path)
tag_vocab = load_vocab(tag_vocab_path)

# id_2_tag_17 = {idx:tag for tag,idx in tag_17_voacb.items()}
id_2_tag_3 = {idx:tag for tag,idx in tag_3_voacb.items()}
id_2_tag_67 = {idx:tag for tag,idx in tag_vocab.items()}
print(id_2_tag_67)

def get_3_id_by_67_id(ids_67):
	ids_3 = []
	for id_67 in ids_67:
		tag_67 = id_2_tag_67[int(id_67)]
		if tag_67[0] == 'B':
			ids_3.append(0)
		elif tag_67[0] == 'I':
			ids_3.append(1)
		elif tag_67[0] == 'O':
			ids_3.append(2)
		else:
			raise Exception('转换3发生错误')
	return ids_3

def get_17_id_by_67_id(ids_67):
	ids_17 = []
	for id_67 in ids_67:
		tag_67 = id_2_tag_67[int(id_67)]
		id_17 = get_new_index(tag_67, tag_17_vocab)
		ids_17.append(id_17)
	return ids_17

def create_mtl_data(original_data_path, mtl_data_path):
	'''
	从original_data_path中读取数据，然后填补两列
	'''
	mtl_file = open(mtl_data_path, 'w')
	original_dataset = dataset(original_data_path, shuffle = False)
	for i,(x_batch, y_batch) in enumerate(original_dataset.get_minibatch(1)):
		tags_67 = y_batch[0]
		# 根据tag来处理得到tag_17, tag_3
		tags_3 = get_3_id_by_67_id(tags_67)
		tags_17 = get_17_id_by_67_id(tags_67)
		# 进行输出
		for token_id, tag_67, tag_17, tag_3 in zip(x_batch[0], tags_67, tags_17, tags_3):
			line = str(token_id) + ' ' + str(tag_67) + ' ' + str(tag_17) + ' ' + str(tag_3)
			mtl_file.write(line + '\n')
		
		mtl_file.write('\n')
#		 break
	mtl_file.close()

def main():

	create_mtl_data(train_data_id_path, train_data_id_mtl_path)
	create_mtl_data(dev_data_id_path, dev_data_id_mtl_path)
	create_mtl_data(test_data_id_path, test_data_id_mtl_path)

if __name__ == '__main__':
	main()