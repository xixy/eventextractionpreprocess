#coding=utf-8
from configure import *
from dataset import *
import sys
from vocab import *

def main():
	'''
	对数据进行预处理，包括vocab等
	'''

	# 首先生成vocab

	# 生成dataset
	train_dataset = dataset(train_data_path)
	#dev_dataset = dataset(dev_data_path)
	#test_dataset = dataset(test_data_path)

	# 获取语料中的tag集合
	#tokens_vocab, tags_vocab = get_vocabs_from_datasets([train_dataset,dev_dataset,test_dataset])
	tokens_vocab, tags_vocab = get_vocabs_from_datasets([train_dataset])

	print('数据集中总共有:' + str(len(tokens_vocab)) + '个字')
	print('数据集中总共有:' + str(len(tags_vocab)) + '个标签')

	# 获取字向量文件中的所有字
	char_vocab = get_vocabs_from_word2vec(char_word2vec_path)
	# 获取词向量文件中的所有词
	#word_vocab = get_vocabs_from_word2vec(word_word2vec_path)

	char_vocab.append(UNK_TOKEN)
	#word_vocab.append(UNK_TOKEN)

	# 将字vocab进行输出
	write_vocab(char_vocab,char_vocab_path)
	write_vocab(tags_vocab,tag_vocab_path)
	#write_vocab(word_vocab,word_vocab_path)

	if os.path.exists(trimmed_char_word2vec_path):
		return 0
	# 提取字向量
	char_vocab = load_vocab(char_vocab_path)
	# 获取Trim word2vec，并进行存储
	export_trimmed_word2vec(char_vocab, char_word2vec_path, trimmed_char_word2vec_path, dimension)

	# 提取字向量
	#word_vocab = load_vocab(word_vocab_path)
	# 获取Trim word2vec，并进行存储
	#export_trimmed_word2vec(word_vocab, word_word2vec_path, trimmed_word_word2vec_path, 256, add_unk = False)

if __name__ == '__main__':
	main()