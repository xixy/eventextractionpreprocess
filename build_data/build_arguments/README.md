数据处理，包括四步：
1. make vocab 处理生成char_vocab, event_type_vocab, argument_role_vocab, argument_role_bio_vocab, entity_type_vocab, entity_sub_type_vocab
2. make data 将数据集进行id表示，暂时不引入语言模型、词、分词信息
得到结果的格式是

event_type_id, argument_role_id
{char_id, entity_sub_type_id, entity_type_id, argument_role_bio_id, distance_1, distance_2}
其中距离是从距离anchor/entity最近的距离开始算起，比如
-2 -1 0 0 1 2 3 4 5 6