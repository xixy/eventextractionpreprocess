#coding=utf-8
from configure import *
from dataset import *

import sys
from vocab import *

def main():
	'''
	对数据进行预处理，包括vocab等
	'''

	# 将文本和标签用index来进行表示
	## 1.1 加载vocab
	char_vocab = load_vocab(char_vocab_path)
	event_type_vocab = load_vocab(event_type_vocab_path)
	argument_role_vocab = load_vocab(argument_role_vocab_path)
	argument_role_bio_vocab = load_vocab(argument_role_vocab_bio_path)
	entity_type_vocab = load_vocab(entity_type_vocab_path)
	entity_sub_type_vocab = load_vocab(entity_sub_type_vocab_path)
	# word_vocab = load_vocab(word_vocab_path)

	char_embeddings = get_trimmed_word2vec(trimmed_char_word2vec_path)

	## 1.2 加载dataset
	# 生成dataset
	train_dataset = dataset(train_data_path)
	dev_dataset = dataset(dev_data_path)
	test_dataset = dataset(test_data_path)
	## 1.3 转化为id文件

	print('最大长度为:' + str(getMaxLength(train_dataset)))
	print('最大长度为:' + str(getMaxLength(dev_dataset)))
	print('最大长度为:' + str(getMaxLength(test_dataset)))

	# 生成有语言模型的id表示
	convert_text_to_id_without_lm_embedding(dev_dataset, dev_data_id_path, \
			char_vocab, event_type_vocab, argument_role_vocab, argument_role_bio_vocab, entity_type_vocab, entity_sub_type_vocab)
	print('dev处理结束')
	convert_text_to_id_without_lm_embedding(test_dataset, test_data_id_path, \
			char_vocab, event_type_vocab, argument_role_vocab, argument_role_bio_vocab, entity_type_vocab, entity_sub_type_vocab)
	print('test处理结束')
	convert_text_to_id_without_lm_embedding(train_dataset, train_data_id_path, \
			char_vocab, event_type_vocab, argument_role_vocab, argument_role_bio_vocab, entity_type_vocab, entity_sub_type_vocab)
	print('train处理结束')

	#3. 添加语言模型向量

	# convert_text_to_id(train_dataset, train_data_id_lm_path, token_vocab, tag_vocab)
	# convert_text_to_id(dev_dataset, dev_data_id_lm_path, token_vocab, tag_vocab)
	# convert_text_to_id(test_dataset, test_data_id_lm_path, token_vocab, tag_vocab)







if __name__ == '__main__':
	main()