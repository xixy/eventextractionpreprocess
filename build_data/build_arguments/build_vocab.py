#coding=utf-8
from configure import *
from dataset import *
import sys
from vocab import *

def main():
	'''
	对数据进行预处理，包括vocab等
	'''

	# 首先生成vocab

	# 生成dataset
	train_dataset = dataset(train_data_path)
	dev_dataset = dataset(dev_data_path)
	test_dataset = dataset(test_data_path)

	# 获取语料中的tag集合
	char_vocab, event_type_vocab, argument_role_vocab, argument_role_bio_vocab, entity_type_vocab, entity_sub_type_vocab \
		= get_vocabs_from_datasets([train_dataset,dev_dataset,test_dataset])
	# tokens_vocab, tags_vocab = get_vocabs_from_datasets([train_dataset])

	print('数据集中总共有:' + str(len(char_vocab)) + '个字')
	print('数据集中总共有:' + str(len(event_type_vocab)) + '个事件类型')
	print('数据集中总共有:' + str(len(argument_role_vocab)) + '个argument roles')
	print('数据集中总共有:' + str(len(entity_type_vocab)) + '个entity types')
	print('数据集中总共有:' + str(len(entity_sub_type_vocab)) + '个entity sub types')


	# 获取字向量文件中的所有字
	char_vocab = get_vocabs_from_word2vec(char_word2vec_path)

	char_vocab.append(UNK_TOKEN)

	# 将字vocab进行输出
	write_vocab(char_vocab,char_vocab_path)

	# 提取字向量
	char_vocab = load_vocab(char_vocab_path)
	# 获取Trim word2vec，并进行存储
	export_trimmed_word2vec(char_vocab, char_word2vec_path, trimmed_char_word2vec_path, dimension)

	# 对tag的逻辑进行检查
	assert (len(argument_role_vocab) - 1) * 2 + 1 == len(argument_role_bio_vocab)

	write_vocab(event_type_vocab, event_type_vocab_path)
	write_vocab(argument_role_vocab, argument_role_vocab_path)
	write_vocab(argument_role_bio_vocab, argument_role_vocab_bio_path)
	write_vocab(entity_type_vocab, entity_type_vocab_path)
	write_vocab(entity_sub_type_vocab, entity_sub_type_vocab_path)


if __name__ == '__main__':
	main()